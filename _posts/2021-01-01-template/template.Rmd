---
title: "Training Title"
description: |
  Training description
author:
  - name: First name Last name
    url: https://example.com/norajones
    affiliation: MaIAGE - Migale - INRAE
    orcid_id: 0000-0001-7517-4724
  - name: First name Last name
    url: https://example.com/norajones
    affiliation: MaIAGE - Migale - INRAE
date: "2021-01-01"
preview: preview.png
categories:
  - category
  - galaxy
  - module X
  - "2021"
output:
  distill::distill_article:
    self_contained: false
    css: [../../css/theme.css,'https://use.fontawesome.com/releases/v5.0.9/css/all.css']
creative_commons: CC BY-SA
bibliography: ../../resources/biblio.bib
csl: ../../resources/biomed-central.csl
---



```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = FALSE)
```

```{r load_packages, message=FALSE, warning=FALSE, include=FALSE} 
library(fontawesome)
```

Retrouvez <a href="https://migale.inrae.fr/sites/default/files/formation/2021/module9bis.pdf">la plaquette</a> de la formation 


Aucun prérequis n'est nécessaire pour cette formation `r fa("exclamation-triangle", fill = "red")`

Programme :

* Construction d’un jeu de données
  - Téléchargement de données publiques
  - Evaluation de la qualité
  - Caractérisation de la diversité génomique
* Stratégies de comparaison :
  - Constructions de famille de protéines
  - Alignement de génomes complets
* Analyse des résultats :
  - Notion de core et pan-génome
  - Notions élémentaires de phylogénomique
  - Visualisation et interprétation des résultats
  
Mise en pratique sur un jeu de données bactériens, utilisation des logiciels Mash et Roary sous Galaxy.

***

Voici tous les supports présentés lors de cette formation :

* [Slides](content/slides.html)
* [TP](content/TP.html)

***



