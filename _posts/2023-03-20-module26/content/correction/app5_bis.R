#
# This is a Shiny web application. You can run the application by clicking
# the 'Run App' button above.
#
# Find out more about building applications with Shiny here:
#
#    http://shiny.rstudio.com/
#

library(shiny)

# Define UI for application that draws a histogram
ui <- fluidPage(
    
    # CSS
    includeCSS("www/styles.css"),

    # Application title
    titlePanel(textOutput("title")),
    
    # Sidebar with a slider input for number of bins 
    sidebarLayout(
        sidebarPanel(
            textInput("title",
                      "Main title :",
                      "Data visualization"),
            sliderInput("bins",
                        "Number of bins:",
                        min = 1,
                        max = 50,
                        value = 30),
            radioButtons("color",
                         "Color of bins:",
                         choices = c("Dark gray" = "darkgray",
                                     "Orange" = "coral",
                                     "Light green" = "lightgreen"))
        ),

        # Show a plot of the generated distribution
        mainPanel(
            tabsetPanel(
                tabPanel(
                    "Histogram",
                    plotOutput("histogram"),
                    textOutput("textBins")
                ),
                tabPanel(
                    "Boxplot",
                    plotOutput("boxplot"),
                ),
                tabPanel(
                    "Table",
                    tableOutput("table"),
                )
            )
        )
    )
)

# Define server logic required to draw a histogram
server <- function(input, output) {
    output$title <- renderText({
        input$title
    })
    
    output$histogram <- renderPlot({
        # generate bins based on input$bins from ui.R
        x    <- faithful[, 2]
        bins <- seq(min(x), max(x), length.out = input$bins + 1)

        # draw the histogram with the specified number of bins
        hist(x, breaks = bins, col = input$color, border = 'white')
    })
    
    output$textBins <- renderText({
        c("There are currently", input$bins, input$color, "bins.")
    })
    
    
    output$boxplot <- renderPlot({
        x    <- faithful[, 2]
        boxplot(x, col = input$color)
    })
    
    output$table <- renderTable({
        x    <- faithful[, 2]
        x
    })
}

# Run the application 
shinyApp(ui = ui, server = server)
