<!DOCTYPE html>
<html lang="" xml:lang="">
  <head>
    <title>Bonnes pratiques pour une meilleure reproductibilité des analyses</title>
    <meta charset="utf-8" />
    <meta name="author" content="Cédric Midoux - Valentin Loux" />
    <meta name="date" content="2023-04-07" />
    <script src="slides_files/header-attrs/header-attrs.js"></script>
    <link href="slides_files/remark-css/default.css" rel="stylesheet" />
    <link href="slides_files/remark-css/default-fonts.css" rel="stylesheet" />
    <link rel="stylesheet" href="css/slides.css”" type="text/css" />
  </head>
  <body>
    <textarea id="source">
class: center, middle, inverse, title-slide

.title[
# Bonnes pratiques pour une meilleure reproductibilité des analyses
]
.subtitle[
## Migale bioinformatics facility
]
.author[
### Cédric Midoux - Valentin Loux
]
.date[
### 2023-04-07
]

---






&lt;style type="text/css"&gt;
.remark-slide-content {
    font-size: 28px;
}
&lt;/style&gt;


# Practical informations


- 9h30 - 17h00

- 2 pauses

- Déjeuner au restaurant INRAE (si vous voulez)

.footnote[
&lt;a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/"&gt;&lt;img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png" /&gt;&lt;/a&gt;&lt;br /&gt;This work is licensed under a &lt;a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/"&gt;Creative Commons Attribution-ShareAlike 4.0 International License&lt;/a&gt;.
]
---

# Tour de table

* Qui êtes vous ?
  - Institution, laboratoire, métier …
* Quelles sont vos approches actuelles concernant la reproductibilité de vos analyses ?
* Connaissez vous le sigle **FAIR** ?
* Avez vous déjà été confrontés à des soucis de reproductibilité d'analyses (les votres, celles d'autres personnes …) ?
* Comment vous sentez vous ? OK / KO

---


# Programme

- Intro : Quelques généralités sur la reproductibilité et la démarche science ouverte

- Organiser son espace de travail 

- Versionner ses documents
  - TP

- Utilisation de documents computationels -- Notebook
  - TP

- Aller vers le FAIR et rédiger son PGD

- Conclusion, discussion et aller + loin

---
# Tout le monde a déjà eu cette expérience

--
.pull-left[
&lt;div class="figure" style="text-align: center"&gt;
&lt;img src="images/monnet-loux-plosone-2010.png" alt="Un article interessant" width="100%" /&gt;
&lt;p class="caption"&gt;Un article interessant&lt;/p&gt;
&lt;/div&gt;
]

--
.pull-right[
&lt;div class="figure" style="text-align: center"&gt;
&lt;img src="images/monnet-loux-plosone-mat-met.png" alt="Un matériel et méthodes décevant" width="100%" /&gt;
&lt;p class="caption"&gt;Un matériel et méthodes décevant&lt;/p&gt;
&lt;/div&gt;
]

---
# Crise de la reproductibilité

- Problème **général**,  "Reproducibility Crisis"
  - Remis en avant par les science sociales, notamment la psychologie
  - Étendu à l'ensemble des disciplines scientifiques

Mais un problème qui n'est **pas nouveau** :
  - Expériences de la pompe à vide au XVIIe siècle (von Guericke et Boyle) 

&lt;div class="figure" style="text-align: center"&gt;
&lt;img src="images/Magdeburger-Halbkugeln.jpg" alt="Experiences des hemisphères de Magdebourg" width="40%" /&gt;
&lt;p class="caption"&gt;Experiences des hemisphères de Magdebourg&lt;/p&gt;
&lt;/div&gt;

???
Au XVIIe, expérience de la pompe à vide par Otto van Guericke étaient basées sur des démonstrations spectaculaires, dans les cours d'Europe qui lui assuraient notoriété. L'expérience des hemisphères de Magdebourg qui consistait à essayer de faire séparer des hémisphères de cuivre dans lesquels il avait fait le vide, par des attelages de chevaux était très appréciée. Cela reposait sur un outillage et un savoir faire secret. Impossible à reproduire
Plus tard, Boyle, sur le même sujet, consignait tout par écrit et avait recours à des témoins de bonne réputation (des gentlemen, à la base de la Royal Society) pour certifier ses expériences. c'est le début des publication scientifiques.
  
---
# Et en bioinfo ?

Un problème vieux comme la bioinformatique :

- En 2009, moins de la moitié des expériences de transcriptomique parues dans Nature Genetics ont pu être reproduites 
- Sur 50 articles citant BWA en 2011, 31 ne citent ni version, ni paramètres. 26 ne donnent pas accès aux données sous-jacentes
- Selon un sondage mené en 2016 auprès de plus de 1500 biologistes
  - 70% ont déjà éprouvé des difficultés à reproduire une analyse &lt;a name=cite-Baker2016&gt;&lt;/a&gt;([Baker, 2016](https://doi.org/10.1038/533452a))

- [Ten Years Reproducibility Challenge](https://github.com/ReScience/ten-years) : êtes vous capables de refaire vos analyses d'il y a 10 ans ? 

---
# Quelles sont les difficultés ?

- Problèmes **d'accès aux données** :
  - le fameux "data available upon request"
  - données brutes disponibles, mais méta-données inexistantes ou insuffisantes
- Problèmes **d'accès aux outils** :
  - outils anciens ou obsolètes
  - difficultés à installer
- Problèmes de **paramètrage de l'analyse**
  - version des outil
  - paramètres des outils
  - enchaînement des outils
- Problème d'accès aux **ressources necessaires**
  - calcul
  - stockage

---
# Réplication ≠ Reproductibilité

- La réplication indépendante d'expériences est à la base de la méthode scientifique

- En complément de **réplication** indépendante ( expérimentation, échantillonnage, analyse, …), la **reproduction** d'analyse est indispensable à l'évaluation et à la compréhension de la démarche employée

- Il existe une ambiguïté en anglais entre réplication (*réplication*) et reproduction (*reproducibility*). Derrière la *reproducibility crisis* on mélange les deux :
  - Impossibilité de répliquer des résultats de façon indépendante (psychologie, médecine, biologie…)
  - Impossibilité de reproduire des analyses à partir des mêmes données de départ

- Chacun peut déjà, par la mise en place de pratiques simples et l'utilisation d'outils conviviaux, améliorer la reproductibilité de ses travaux

Source : &lt;a name=cite-allard&gt;&lt;/a&gt;([Allard, 2018](https://laviedesidees.fr/La-crise-de-la-replicabilite.html)) 
&lt;!-- https://laviedesidees.fr/La-crise-de-la-replicabilite.html --&gt;

---
# En pratique, qu'est ce qu'être reproductible ?


&lt;img src="images/reproducible-data-analysis.png" width="80%" style="display: block; margin: auto;" /&gt;
&lt;!--https://github.com/karthik/rstudio2019/blob/master/reproducible-data-analysis.pdf--&gt;
---
# En pratique, qu'est ce qu'être reproductible (2) ?

  Avoir accès :
  - aux pièces (les **données**)
  - aux outils ( les **logiciels**, )
  - au mode d'emploi : **paramètres**, **workflows d'analyse**
  
  Mais aussi :
  - à la description des pièces, de la façon dont elles ont été produites (**méta-données**)
  - à la documentation technique (**choix techniques explicites**)
  - au savoir faire du monteur (**formations**)
  - Éventuellement à un atelier équipé pour le montage (**ressources informatiques**)

---

# FAIR : un pré-requis à la reproductibilité
&lt;div class="figure" style="text-align: center"&gt;
&lt;img src="images/fair.png" alt="Findale Accessible Interoperable Reusable " width="40%" /&gt;
&lt;p class="caption"&gt;Findale Accessible Interoperable Reusable &lt;/p&gt;
&lt;/div&gt;

---

# FAIR : un pré-requis à la reproductibilité
Principes autour des données au sens large :
- **F**acile à trouver : pour les humains et les ordinateurs :
  - id unique et pérennes
  - métadonnées riches
- **A**ccessibles à ** long terme**
  - entrepôt "pérenne"
  - licence d'utilisation explicite ( FAIR ≠ ouvert)
- **I**nteroperables : faciles à combiner avec d'autres jeux de données
  - formats ouverts et documentés
  - vocabulaire standardisé, ontologies (données et méta-données)
- **R**éutilisables :
  - réutilisables par soi, par d'autres
  - réutilisables par des machines

&lt;a name=cite-Wilkinson2016&gt;&lt;/a&gt;([Wilkinson, Dumontier, Aalbersberg, Appleton, Axton, Baak, Blomberg, Boiten, da
Silva Santos, Bourne, Bouwman, Brookes, Clark, Crosas, Dillo, Dumon, Edmunds, Evelo, Finkers, Gonzalez-Beltran, Gray, Groth, Goble, Grethe, Heringa, Hoen, Hooft, Kuhn, Kok, Kok, Lusher, Martone, Mons, Packer, Persson, Rocca-Serra, Roos, van
Schaik, Sansone, Schultes, Sengstag, Slater, Strawn, Swertz, Thompson, van der
Lei, van
Mulligen, Velterop, Waagmeester, Wittenburg, Wolstencroft, Zhao, and Mons, 2016](https://doi.org/10.1038/sdata.2016.18)) 

---

# Le spectre de la reproductibilité

&lt;div class="figure" style="text-align: center"&gt;
&lt;img src="images/reproducibility-spectrum.png" alt="Spectre de la reproductibilité," width="100%" /&gt;
&lt;p class="caption"&gt;Spectre de la reproductibilité,&lt;/p&gt;
&lt;/div&gt;
Source : &lt;a name=cite-piazzi&gt;&lt;/a&gt;([Piazzi, Cerqueira, Manso, and Duque, 2018](#bib-piazzi)) 

---
# En pratique, que faire, quels outils utiliser ? (1/2)

Aller de façon **pragmatique** vers une documentation accrue de ce que l'on fait (comment, pourquoi) et des données que l'on utilise et produit. 

- Rendre accessible ses données à soit, aux partenaires, à tous) :
  - Documenter collectivement ses pratiques de gestion de données (responsabilités, formats, cycle de vie, archivage…) dns un Data Management Plan qui doit être **vivant** et partagé.(Opidor) 
  - Dépôt internationaux (ENA, NCBI) pour les données specialisées
  - DataVerse, Figshare, Zenodo ou autre  pour les autres données

- Définir et fixer les versions des outils utilisés :
  - Conda, Bioconda
  - Singularity, Docker
  - Machine Virtuelle
  
---
# En pratique, que faire, quels outils utiliser ? (2/2)

- Décrire son workflow d'analyse, le rendre portable : 
  - Galaxy
  - Snakemake, Nextflow

- Gérer les versions de ses codes, les publier :
  - git
  - GitHub / GitLab

- Tracer son analyse dans des documents computationnels partageables et réutilisables :
    - Rmd
    - Jupyter Notebooks

---
# Objectifs du TP

Décomplexifier les problème, se décomplexifier sur ses pratiques , désacraliser  la reproductibilité !

Vous fournir des outils, des pistes pour rendre vos projets :
- transparents
- robustes
- réutilisables
- partageables

Bref, *plus* ouverts et reproductibles.


Parties pratiques sur la versionning des documents (Git et GitHub) et les documents  computationnels.

---
class: inverse, center, middle
# Organiser son espace de travail 

---
# Organiser son espace de travail 
.pull-left[
![](https://journals.plos.org/ploscompbiol/article/figure/image?size=large&amp;id=info:doi/10.1371/journal.pcbi.1000424.g001)
Source : &lt;a name=cite-noble&gt;&lt;/a&gt;([Noble, 2009](https://doi.org/10.1371/journal.pcbi.1000424))  

]

.pull-right[
Séparer :
- données
- code
- scripts
- résultats
- Avoir un copie de sauvegarde de ses données
- Mettre le répertoire et les fichiers de données en lecture seule
- Avoir une convention de nommage de ses fichiers
]

---
# Organiser son espace de travail (2)

&lt;img src="images/Infographic_Data_Management_v02-02_KH.png" width="50%" style="display: block; margin: auto;" /&gt;

Source : [Twitter Kira Höffler](https://twitter.com/KiraHoeffler/status/1367804034413920259) | [Version PDF](images/Infographic_Data_Management_v02-02_KH.pdf) 

---
class: inverse, center, middle
# Versionner ses documents 

---
# Pourquoi versionner ses projets ?

.pull-left[
&lt;div class="figure" style="text-align: center"&gt;
&lt;img src="images/phd101212s.gif" alt="Piled Higher and Deeper by Jorge Cham. phdcomics" width="60%" /&gt;
&lt;p class="caption"&gt;Piled Higher and Deeper by Jorge Cham. phdcomics&lt;/p&gt;
&lt;/div&gt;
]
--

### &amp;rarr; Suivre l'évolution des fichiers
* garder en mémoire chaque modification de chaque fichier
* pourquoi elle a eu lieu
* quand et par qui !

--

### &amp;rarr; Faciliter le développement collaboratif
* fusionne les différentes modifications

--

### &amp;rarr; Revenir à une version précédente
* et assure une sauvegarde de son travail.


&lt;!--Voici un [aide mémoire git](https://github.github.com/training-kit/downloads/fr/github-git-cheat-sheet.pdf)--&gt;

---
# Vocabulaire
.pull-left[
Quelques concepts à connaitre :
- **Repository** = Dépôt. Dossier contenant tous les fichiers d'un projet. Personnel ou partagé. Public ou privé. Local ou distant (remote).

- **Commit** = Enregistrement d'un ensemble de fichier à un instant T (= photo) 

- **Branche** = Ensemble chaîné de commits, par défaut la branche principale s'appelle « main » 
]

.pull-right[
- **Git** : logiciel *open-source*  de gestion de version de document. Il est tout a fait possible d'utiliser git pour versionner ses documents sans GitHub.
- **GitHub** : site web permettant de centraliser en ligne ses dépôts git et facilitant la collaboration sur les projets.

&lt;img src="images/git-github.png" width="50%" style="display: block; margin: auto;" /&gt;
]

---
# Travailler en commun avec Git et Github :

.center[![Github Flow](images/github-flow.png)] 


- **Branche** = version parallèle à la version principale
- **Pull Request = demande de fusion des modifications d'une branche vers la branche principale

[Un exemple de PR](https://github.com/galaxyproject/training-material/pull/1354) 


---
class: center, middle, inverse

# TP : Utilisation de Git et GitHub

---

# Git : en ligne de commandes 

- `git clone` : cloner un dépot distant
- `git init` : initialiser le versionning sur un dépôt local
- `git commit` : enregistrer l'état d'un dépôt
- `git status` : afficher l'état des documents du dépôt
- `git diff` : comparer l'état actuel au dernier commit, ou deux commits entre eux ou deux branches 
- `git pull` : récupérer les commits distants
- `git push` : envoyer les commits locaux

et bien d'autres encore (`blame`, `revert`,…)

&gt; [CheatSheet](https://github.github.com/training-kit/downloads/github-git-cheat-sheet.pdf) et [VF](https://training.github.com/downloads/fr/github-git-cheat-sheet.pdf)  
&gt; [Happy git with R](https://happygitwithr.com/index.html)

---
class: inverse, center, middle
# Des langages à faible balisage pour faciliter la traçabilité et la prise de note

---
# Des langages à faible balisage 

*Comment mettre en forme et structurer simplement un document texte ?*  

--
&amp;rarr; avec un balisage faible tel que *Markdown* 

--
- Permet :

  - Organiser les titres de sections
  - Italique / gras / souligné
  - Générer des listes
  - Ajouter des tableaux
  - Insertion d'image et de blocs de code

--

- Texte codé en UTF-8 (assure une pérennité, lisibilité et portabilité) facilement versionnable.


---
# Markdown - exemples de mise en forme 


````bash
# Titre H1
## Sous-titre H2
### Sous-titre H3
*italique*, **gras** et `code`
&gt; Citations

```
bloc de code
```

* liste
  * item
  * item
  
[lien](https://fr.wikipedia.org)
![](https://migale.inrae.fr/sites/default/files/migale.png) 
      #lien vers une image en ligne ou dans l'espace de travail
````

Gardez ce [mémo](https://guides.github.com/pdfs/markdown-cheatsheet-online.pdf) à porté de main !

---

# *Exemple illustrant la simplification du balisage*

.pull-left[
- HTML


```bash
  &lt;ul&gt;
    &lt;li&gt;item1&lt;/li&gt;
    &lt;li&gt;item2&lt;/li&gt;
  &lt;/ul&gt;
```
]

.pull-right[
- Markdown


```bash
  - item1
  - item2
```
]

---
class: inverse, center, middle
# Documents computationels - Notebook

---
# Documents computationels - Notebook

- Il faut se donner les moyens pour qu’autrui puisse inspecter nos analyses
- Expliciter pour augmenter les chances de trouver les erreurs et de les éliminer
  - Inspecter pour justifier et comprendre
  - Refaire pour vérifier, corriger et réutiliser

&lt;img src="https://blog.f1000.com/wp-content/uploads/2014/04/reproducibility-small-480x324.jpg" width="45%" style="display: block; margin: auto;" /&gt;


---
# Documents computationels - Notebook

- Regrouper dans *un unique document*: 
  - Les informations, le code, calculs et les résultats 
  - Pour assurer leur cohérence et améliorer la traçabilité. 
  - Exportable (ex : html) pour une meilleure portabilité et lisibilité.

&lt;img src="images/Reproducible-Research.jpg" width="45%" style="display: block; margin: auto;" /&gt;

&lt;a name=cite-russo&gt;&lt;/a&gt;([Russo, Righelli, and Angelini, 2016](https://doi.org/10.1007/978-3-319-44332-4_19))

&gt; Encore un joli [mémo](https://www.rstudio.org/links/r_markdown_cheat_sheet) pour R markdown. 

---
class: center, middle, 

.pull-left[![](https://github.com/ecoinfAEET/Reproducibilidad/raw/master/figuras/Fig3_components/Rmd.png)]

.pull-right[![](https://github.com/ecoinfAEET/Reproducibilidad/raw/master/figuras/Fig3_components/Pdf.png)]


---
# Nouveauté 2023 : Quarto

&gt; Une alternative open-source pour la publication de documents scientifiques et techniques !

- Input :`Python`, `R`, `Julia`, et  `Observable`
- Output : Articles, présentations, livres, sites/blogs, ...
- Rédaction et déploiement simplifié 


&lt;img src="https://github.com/allisonhorst/stats-illustrations/raw/main/julie-mine-quarto-keynote/quarto_schematic.png" width="40%" style="display: block; margin: auto;" /&gt;

[Source](https://quarto.org)

---
class: center, middle, inverse

# TP : Documents computationels  
---

# Documents computationels : En résumé 

**Dans un unique document on a** :
- Une entête générale
- Du texte, mis en forme avec markdown
- Du code R ou Python (ou autre) dans des chunks
- Des résultats, plots et outputs 

* Ce document est versionné
* dans un format texte dont la lisibilité est assurée au cours du temps
* exportable en HTML et accessible via les GitHub Pages.

&gt; ** &amp;rarr; Expliciter pour augmenter les chances de trouver les erreurs et de les éliminer **
&gt;  - Inspecter pour justifier et comprendre
&gt;  - Refaire pour vérifier, corriger et réutiliser

---
class: inverse, center, middle
# Aller vers le FAIR et rédiger son PGD

---
# Science Ouverte

### Second Plan Nation Science Ouverte (2022)

La **science ouverte** est la diffusion sans entrave des **publications** et des **données de la recherche**. Elle s’appuie sur l’opportunité que représente la mutation numérique pour développer l’accès ouvert aux publications et – autant que possible – aux données de la recherche.

[Source](https://www.ouvrirlascience.fr/plan-national-pour-la-science-ouverte/)
---

### Exigences des financeurs

#### Open Access :
- Obligation to deposit Pre-print  in Open repositories (HAL or equivalent)
  - Favor Gold (at least) Open Access Journals
  - Favor Creative Commons "CC-BY" license

####  Open Research Data :
  - Alignement on European and National strategy
  - « As open As Possible, as Closed as Necessary »   


---
#Plan de Gestion de Données 

## Quoi ?
.pull-right[![Data Lifecycle](images/datalifecycle.png)]

Un document *¨collaboratif** qui définit la façon dont les données seront **gérées"** **pendant** et **après** le projet. 

**Objectifs**
Penser à toutes les étapes du cycle de vie de la donnée

**Avantages ** 
Le modèle vous aide à anticiper l'ensemble des questions et des problèmes qui peuvent se poser par le biais d'une séride  de questions. 


---
#Plan de Gestion de Données (2)
### Quand ?
Document **évolutif**. Au moins 3 versions :
- Une première version au début du projet
- Des mises à jour régulières tout au long du projet
- Une version finale en fin de projet

### Qui
l'ensemble des partenaires. PGD = *pour générer du dialogue*.

---
# Pourqoi un PGD ?

## Plan :
On **planifie** , on anticipe

## Gestion :
On gère, on améliore et on commence par ne plus perdre de données.

## Données
Quelles sont les données **critiques** généres ou utilisées dans le projet ?

---
### Objectifs du PGD
.pull-left[
&lt;small&gt;
- Assurer la reproductibilité des expériences
    - Décrire comment les données sont obtenues
- Faciliter la réutilisation des données
    - Assurer la bonne compréhension des données
- Respecter la loi et les individus
    - Clarifier le cadre légal et éthique
- Éviter la perte de données
    - Assurer un stockage approprié (à court et à long terme)
- Clarifier les droits de réutilisation
    - Préciser les conditions de partage
- Établir le rôle de chaque personne
    - Définir les responsabilités
&lt;/small&gt;
]
.pull-right[
&lt;small&gt;
Pour vous et vos collaborateurs :
- Se référer au DMP pour :
- Récupérer des données
- Comprendre les données
- Savoir où se trouvent les données

Pour la communauté scientifique, publiez le DMP pour indiquer : 
- Quelles données ont été générées dans le cadre du projet
- Où elles sont publiées
- Qui peut y accéder, sous quelles conditions...


&lt;/small&gt;
]


---
# PGD en pratique
.pull-left[
Différents modèles (ANR, Horizon Europe)
- Un seul document avec 6 sections, 2 à 4 questions par section.
- DMP OPIDoR pour organiser l'écriture collaborative
]
.pull-right[
![Singularity](images/dmp-opidor.png)
![Singularity](images/opidor-logo.png)

[DMP Opidor](https://dmp.opidor.fr/)

]

---
# DMP Section
## Modèle ANR
Informations générales sur le projet 
- Financement, date
- Partenaires
- Contributeurs
    - Nom, contact et rôles
- Produits de recherche du projet 

---
# Produit de recherche ?

Selon l’OCDE, les données scientifiques (research data) sont « des enregistrements factuels (chiffres, textes, images et sons), qui sont utilisés comme sources principales pour la recherche scientifique et sont généralement reconnus par la communauté scientifique comme nécessaires pour valider des résultats de recherche. »


[Source](https://rdm.elixir-belgium.org/data_in_dmp.html#data-documentation-in-dmp)
[Source](https://anr.fr/fr/lanr/engagements/la-science-ouverte/)

---
# Contenu du DMP
1 Informations générales
2 Description des données
3 Documentation et qualité des données
4 Stockage et sauvegarde pendant le processus de recherche
5 Exigences légales et éthiques
6 Partage des données et conservation à long terme
7 Responsabilités et ressources en matière de gestion des données.

Explorons un [exemple de DMP](https://dmp.opidor.fr/public_plans)
---


class: inverse, center, middle
# Pour aller + loin

---
# Fixer et partager son environnement

- Conda et  Bioconda 
  - gestion des dépendances, versions
  - Possibilité de créer un environnement par analyse
  - Exporter son environnement dans un fichier `env.yml` et le versionner
  - `conda env export &gt; environment.yml`

- Containers,  machines virtuelles
  - Docker, Singularity, VM virtualbox
  - Pour les outils non "conda-isables", les environnements complexes
  - Les images Singularity sont déployables sur les infrastructures type IFB et s'éxecutent "presque" comme un executable

---
# Fixer et partager son environnement (2) 
.center[![Singularity](https://ars.els-cdn.com/content/image/1-s2.0-S2405471218301406-gr1.jpg)]

&lt;a name=cite-Bjorn2018&gt;&lt;/a&gt;([Grüning, Chilton, Köster, Dale, Soranzo, van den
Beek, Goecks, Backofen, Nekrutenko, and Taylor, 2018](https://doi.org/10.1016/j.cels.2018.03.014)) 

---
# Gestionnaires de workflows

Snakemake, Nextflow pour :
- Définir de façon "simple" et modulaire des workflows d'analyse :
  - Parallelisables : les étapes indépendantes peuvent être jouées en parallèle.
  - Qui assurent la reprise sur erreur : si on refait une analyse, change un paramètre, seul ce qui doit être rejoué est relancé.
  - Portables : un même script peut être joué en local, sur des  clusters différents en changeant le fichier de configuration.
  - Partageables : un fichier texte versionné
  - Peut gérer pour vous le versionning et l'installation des outils avec Conda

---
# Gestionnaires de workflows

.center[![](images/snakemake-schema.png)]

---
# exemple de `Snakefile`

**Bash**


```bash
 for sample in `ls *.fastq.gz` do
  fastqc ${sample}
done
```

--

**Snakefile**


```bash
SAMPLES, = glob_wildcards("./{sample}.fastq.gz")

rule final: 
  input:expand("fastqc/{sample}/{sample}_fastqc.zip",smp=SAMPLES)

rule fastqc:
  input: "{sample}.fastq.gz"
  output: "fastqc/{sample}/{sample}_fastqc.zip"
  conda: "fastqc.yaml"
  message: """Quality check"""
  shell: """fastqc {input} --outdir fastqc/{wildcards.sample}"""
```
---
# Pour aller + loin - FAIRifier ses données
Dépôts dans les dépôts publics :
- Dans les dépôts thématiques internationaux (européens !)  
    - données brutes 
    - données analysées
    - /!\ méta-données
- dans les dépots généralistes (dataverse , figshare, …)
  - fichiers tabulés; "autres" données. ce qu'on mettrait en suypplementary material.
  - (éventuels) liens vers les fichiers de données

- Publier un data-paper ? et un protocol ?

---
# Pour aller + loin - Mat &amp; Met
.pull-left[
- jusqu'où aller dans la reproductibilité ?
  - Mat et Met électroniques :
    - Galaxy Pages
    - Gigascience, GigaDB : 
      - "GigaScience aims to revolutionize publishing by promoting reproducibility of analyses and data dissemination, organization, understanding, and use. "

]
.pull-right[
&lt;div class="figure" style="text-align: center"&gt;
&lt;img src="images/galaxy-pages.png" alt="Live Mat et Met https://usegalaxy.org/u/aun1/p/windshield-splatter" width="80%" /&gt;
&lt;p class="caption"&gt;Live Mat et Met https://usegalaxy.org/u/aun1/p/windshield-splatter&lt;/p&gt;
&lt;/div&gt;

Au final, toujours se poser la question du rapport coût / bénéfice.
]

---
class: center, middle

![](images/ikea-win.png)




---
# Ressources

- [FUN MOOC Recherche Reproductible](https://www.fun-mooc.fr/courses/course-v1:inria+41016+self-paced/about)
- [FAIR Bioinfo](https://github.com/thomasdenecker/FAIR_Bioinfo)
- [Cours Git et Github](https://guides.github.com)
- [Github pages](https://pages.github.com)
- [Rmd the definitive Guide](https://bookdown.org/yihui/rmarkdown/)
- [Snakemake](https://snakemake.readthedocs.io/en/stable/)
- [NextFlow](https://www.nextflow.io) et [nf-core](https://nf-co.re)
- 

- Les mémo présentés dans ce cours :
  - [markdown](https://guides.github.com/pdfs/markdown-cheatsheet-online.pdf)
  - [git](https://github.github.com/training-kit/downloads/fr/github-git-cheat-sheet.pdf)
  - [R markdown](https://www.rstudio.org/links/r_markdown_cheat_sheet)


---
# Ressources
IFB course « FAIR data 2022 : principes FAIR pour la gestion des données de recherche en sciences de la vie » https://moodle.france-bioinformatique.fr/course/view.php?id=8

ANR DMP template documentation : https://anr.fr/fileadmin/documents/2019/ANR_DMP_Template_EN.pdf

French National 2nd Open Science Plan https://www.ouvrirlascience.fr/second-national-plan-for-open-science/


---

# References
&lt;a name=bib-allard&gt;&lt;/a&gt;[Allard, A.](#cite-allard) (2018). _La crise de
la réplicabilité_. URL:
[https://laviedesidees.fr/La-crise-de-la-replicabilite.html](https://laviedesidees.fr/La-crise-de-la-replicabilite.html).

&lt;a name=bib-Baker2016&gt;&lt;/a&gt;[Baker, M.](#cite-Baker2016) (2016). "1, 500
scientists lift the lid on reproducibility". In: _Nature_ 533.7604, pp.
452-454. DOI: [10.1038/533452a](https://doi.org/10.1038%2F533452a).
URL:
[https://doi.org/10.1038/533452a](https://doi.org/10.1038/533452a).

&lt;a name=bib-Bjorn2018&gt;&lt;/a&gt;[Grüning, B., J. Chilton, J. Köster, et
al.](#cite-Bjorn2018) (2018). "Practical Computational Reproducibility
in the Life Sciences". In: _Cell Systems_ 6.6, pp. 631-635. DOI:
[10.1016/j.cels.2018.03.014](https://doi.org/10.1016%2Fj.cels.2018.03.014).
URL:
[https://doi.org/10.1016/j.cels.2018.03.014](https://doi.org/10.1016/j.cels.2018.03.014).

&lt;a name=bib-noble&gt;&lt;/a&gt;[Noble, W. S.](#cite-noble) (2009). "A Quick
Guide to Organizing Computational Biology Projects". In: _PLOS
Computational Biology_ 5.7, pp. 1-5. DOI:
[10.1371/journal.pcbi.1000424](https://doi.org/10.1371%2Fjournal.pcbi.1000424).
URL:
[https://doi.org/10.1371/journal.pcbi.1000424](https://doi.org/10.1371/journal.pcbi.1000424).

&lt;a name=bib-piazzi&gt;&lt;/a&gt;[Piazzi, A. C., A. S. Cerqueira, L. R. Manso, et
al.](#cite-piazzi) (2018). "Reproducible research platform for electric
power quality algorithms" , pp. 1-6.

&lt;a name=bib-russo&gt;&lt;/a&gt;[Russo, F., D. Righelli, and C.
Angelini](#cite-russo) (2016). "Advantages and Limits in the Adoption
of Reproducible Research and R-Tools for the Analysis of Omic Data".
In: Vol. 9874. , pp. 245-258. DOI:
[10.1007/978-3-319-44332-4_19](https://doi.org/10.1007%2F978-3-319-44332-4_19).

&lt;a name=bib-Wilkinson2016&gt;&lt;/a&gt;[Wilkinson, M. D., M. Dumontier, I. J.
Aalbersberg, et al.](#cite-Wilkinson2016) (2016). "The FAIR Guiding
Principles for scientific data management and stewardship". In:
_Scientific Data_ 3.1. DOI:
[10.1038/sdata.2016.18](https://doi.org/10.1038%2Fsdata.2016.18). URL:
[https://doi.org/10.1038/sdata.2016.18](https://doi.org/10.1038/sdata.2016.18).
    </textarea>
<style data-target="print-only">@media screen {.remark-slide-container{display:block;}.remark-slide-scaler{box-shadow:none;}}</style>
<script src="https://remarkjs.com/downloads/remark-latest.min.js"></script>
<script>var slideshow = remark.create({
"ratio": "16:9"
});
if (window.HTMLWidgets) slideshow.on('afterShowSlide', function (slide) {
  window.dispatchEvent(new Event('resize'));
});
(function(d) {
  var s = d.createElement("style"), r = d.querySelector(".remark-slide-scaler");
  if (!r) return;
  s.type = "text/css"; s.innerHTML = "@page {size: " + r.style.width + " " + r.style.height +"; }";
  d.head.appendChild(s);
})(document);

(function(d) {
  var el = d.getElementsByClassName("remark-slides-area");
  if (!el) return;
  var slide, slides = slideshow.getSlides(), els = el[0].children;
  for (var i = 1; i < slides.length; i++) {
    slide = slides[i];
    if (slide.properties.continued === "true" || slide.properties.count === "false") {
      els[i - 1].className += ' has-continuation';
    }
  }
  var s = d.createElement("style");
  s.type = "text/css"; s.innerHTML = "@media print { .has-continuation { display: none; } }";
  d.head.appendChild(s);
})(document);
// delete the temporary CSS (for displaying all slides initially) when the user
// starts to view slides
(function() {
  var deleted = false;
  slideshow.on('beforeShowSlide', function(slide) {
    if (deleted) return;
    var sheets = document.styleSheets, node;
    for (var i = 0; i < sheets.length; i++) {
      node = sheets[i].ownerNode;
      if (node.dataset["target"] !== "print-only") continue;
      node.parentNode.removeChild(node);
    }
    deleted = true;
  });
})();
// add `data-at-shortcutkeys` attribute to <body> to resolve conflicts with JAWS
// screen reader (see PR #262)
(function(d) {
  let res = {};
  d.querySelectorAll('.remark-help-content table tr').forEach(tr => {
    const t = tr.querySelector('td:nth-child(2)').innerText;
    tr.querySelectorAll('td:first-child .key').forEach(key => {
      const k = key.innerText;
      if (/^[a-z]$/.test(k)) res[k] = t;  // must be a single letter (key)
    });
  });
  d.body.setAttribute('data-at-shortcutkeys', JSON.stringify(res));
})(document);
(function() {
  "use strict"
  // Replace <script> tags in slides area to make them executable
  var scripts = document.querySelectorAll(
    '.remark-slides-area .remark-slide-container script'
  );
  if (!scripts.length) return;
  for (var i = 0; i < scripts.length; i++) {
    var s = document.createElement('script');
    var code = document.createTextNode(scripts[i].textContent);
    s.appendChild(code);
    var scriptAttrs = scripts[i].attributes;
    for (var j = 0; j < scriptAttrs.length; j++) {
      s.setAttribute(scriptAttrs[j].name, scriptAttrs[j].value);
    }
    scripts[i].parentElement.replaceChild(s, scripts[i]);
  }
})();
(function() {
  var links = document.getElementsByTagName('a');
  for (var i = 0; i < links.length; i++) {
    if (/^(https?:)?\/\//.test(links[i].getAttribute('href'))) {
      links[i].target = '_blank';
    }
  }
})();</script>

<script>
slideshow._releaseMath = function(el) {
  var i, text, code, codes = el.getElementsByTagName('code');
  for (i = 0; i < codes.length;) {
    code = codes[i];
    if (code.parentNode.tagName !== 'PRE' && code.childElementCount === 0) {
      text = code.textContent;
      if (/^\\\((.|\s)+\\\)$/.test(text) || /^\\\[(.|\s)+\\\]$/.test(text) ||
          /^\$\$(.|\s)+\$\$$/.test(text) ||
          /^\\begin\{([^}]+)\}(.|\s)+\\end\{[^}]+\}$/.test(text)) {
        code.outerHTML = code.innerHTML;  // remove <code></code>
        continue;
      }
    }
    i++;
  }
};
slideshow._releaseMath(document);
</script>
<!-- dynamically load mathjax for compatibility with self-contained -->
<script>
(function () {
  var script = document.createElement('script');
  script.type = 'text/javascript';
  script.src  = 'https://mathjax.rstudio.com/latest/MathJax.js?config=TeX-MML-AM_CHTML';
  if (location.protocol !== 'file:' && /^https?:/.test(script.src))
    script.src  = script.src.replace(/^https?:/, '');
  document.getElementsByTagName('head')[0].appendChild(script);
})();
</script>
  </body>
</html>
