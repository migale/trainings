[TOC]

https://www.nature.com/articles/s41597-019-0287-z.pdf

> - Données
> - Outils
>
> - Résultats

# Organiser son espace de travail

On commence par créer et organiser notre dossier pour le TP.

```sh
cd work/
mkdir formationShotgun/
cd formationShotgun/
mkdir DATA/
mkdir log/
```

# Télécharger les séquences

Les données sont disponibles au NCBI : https://www.ncbi.nlm.nih.gov/sra/SRR8073716.

On utilise `fasterq-dump` de SRA Toolkit pour les récupérer.  

```sh
conda activate sra-tools-2.10.1
# fasterq-dump SRR8073716 --outdir DATA --temp /projet/tmp --verbose --threads 1 > log/fasterq-dump.log & #--split-3 --skip-technical (by default) # --> https://github.com/ncbi/sra-tools/issues/171
fastq-dump SRR8073716 --outdir DATA --split-3 --skip-technical --disable-multithreading > log/fastq-dump.log &
conda deactivate
```

<!--2h-->  

> *Note :* Les nœuds n'étant pas connectés à internet, on travail directement sur le frontal. On se limite à un unique cœur pour ne pas pénaliser les autres utilisateurs. Pour les calculs suivants, on passera obligatoirement par le gestionnaire de jobs (pour les compléments sur ce point, voir  [le tutoriel dédié](https://migale.inrae.fr/cluster))

Les données ne sont pas compressées, il est nécessaire de systématiquement travailler avec des données compressées lorsque cela est possible

```sh
qsub -cwd -V -N gzip -o log/ -e log/ -b y "gzip --verbose DATA/*.fastq"
```

<!--2h15-->  

> Pour le subset de 10Mreads
>
> ```sh
> cd /home/cmidoux/work/Shotgun10
> 
> qsub -cwd -V -N subsetR1 -o .log/out -e .log/err -q maiage.q -pe thread 8 -b y "conda activate seqkit-0.11.0 && seqkit sample -s12345 -n10000000 /home/cmidoux/work/formationShotgun/DATA/SRR8073716_1.fastq.gz -o /home/cmidoux/work/Shotgun10/DATA/raw/mock_R1.fastq.gz --threads 8 && conda deactivate"
> qsub -cwd -V -N subsetR2 -o .log/out -e .log/err -q maiage.q -pe thread 8 -b y "conda activate seqkit-0.11.0 && seqkit sample -s12345 -n10000000 /home/cmidoux/work/formationShotgun/DATA/SRR8073716_2.fastq.gz -o /home/cmidoux/work/Shotgun10/DATA/raw/mock_R2.fastq.gz --threads 8 && conda deactivate"
> ```
>
> Données traités indépendamment dans le dossier `/home/cmidoux/work/Shotgun10/` avec le pipeline [workflow_metagenomics.git](https://gitlab.irstea.fr/cedric.midoux/workflow_metagenomics/-/tree/master). 

# Contrôle des données brutes

## Contrôle qualité avec `fastqc`

Comme pour tous les projets avec des données omiques, on commence par observer les métriques de qualité du jeu de donnée avec `fastqc`. L'outils `multiqc` permet de regrouper les rapports d'analyses de nombreux outils dont `fastqc`.

```sh
mkdir 1_FASTQC/
qsub -cwd -V -N fastqc -pe thread 4 -o log/ -e log/ -b y "conda activate fastqc-0.11.8 && fastqc --outdir 1_FASTQC/ --threads 4 DATA/*.fastq.gz && conda deactivate"
qsub -cwd -V -N multiqc -o log/ -e log/ -b y "conda activate multiqc-1.8 && multiqc --outdir 1_FASTQC/ 1_FASTQC/ && conda deactivate"
firefox --no-remote 1_FASTQC/multiqc_report.html
```

<!--10min-->  

## Assignation taxonomique des données brutes

On utilise `kaiju` pour annoter taxonomiquement les reads brutes. Cette annotation se fait avec la banque *NCBI nr*. 

==Utilisation de `-l h_vmem=10G` à valider==

```sh
mkdir 2_KAIJU/
qsub -cwd -V -N kaiju -q long.q -pe thread 10 -l h_vmem=10G -R y -o log/ -e log/ -b y "conda activate kaiju-1.7.3 && kaiju -t /db/outils/kaiju/nr/nodes.dmp -f /db/outils/kaiju/nr/kaiju_db_nr_euk.fmi -i DATA/SRR8073716_1.fastq.gz -j DATA/SRR8073716_2.fastq.gz -o 2_KAIJU/SRR8073716.kaijuNR -z 10 && conda deactivate"
qsub -cwd -V -N krona -o log/ -e log/ -b y "conda activate kaiju-1.7.3 && kaiju2krona -t /db/outils/kaiju/nr/nodes.dmp -n /db/outils/kaiju/nr/names.dmp -i 2_KAIJU/SRR8073716.kaijuNR -o 2_KAIJU/SRR8073716.krona -u && ktImportText -o 2_KAIJU/SRR8073716.html 2_KAIJU/SRR8073716.krona && conda deactivate"
firefox --no-remote 2_KAIJU/SRR8073716.html
```

> *Note :* Ce job est long, on privilégie donc la queue associé. D'autre part, il nécessite beaucoup de mémoire. On réserve et limite ici l'utilisation à $10 CPU * 10 GB = 100 GB$ 

<!--1h-->  

## Clusterisation des échantillons

==on ajoute 2 autres echantillons randoms pour la CAH ?==

```sh
simka
```

## Nettoyage préprocessing

### Nettoyage  *all-in-one* avec `fastp`

Il existe de nombreux outils, plus ou moins spécifiques, pour le nettoyage des données brutes. Pour ce TP nous utilisons `fastp` qui est un outil *all-in-one*.  Nous indiquons en paramètres les données brutes ainsi que la destination des données nettoyées et du rapport. Les rapport `fastp` peuvent également être compilé avec `multiqc`.

```sh
mkdir 3_FASTP/
qsub -cwd -V -N fastp -pe thread 4 -o log/ -e log/ -b y "conda activate fastp-0.20.0 && fastp --in1 DATA/SRR8073716_1.fastq.gz --in2 DATA/SRR8073716_2.fastq.gz --out1 3_FASTP/SRR8073716_R1.fastq.gz --out2 3_FASTP/SRR8073716_R2.fastq.gz --verbose --length_required 50 --html 3_FASTP/SRR8073716_fastp.html --json 3_FASTP/SRR8073716_fastp.json --thread 4 && conda deactivate"
firefox --no-remote 3_FASTP/SRR8073716_fastp.html
```

<!--30min-->  

### Séparation des rRNA

==mise a jour des refs dans `/db/outils/sortmerna` ?==

==`/db/silva/current/fasta/` est vide ?!==

```sh
mkdir 4_SORTMERNA/
qsub -cwd -V -N sortmerna -pe thread 4 -o log/ -e log/ -b y "conda activate sortmerna-4.2.0
sortmerna 
# --ref /usr/local/genome/src/sortmerna-2.0/rRNA_databases/rfam-5s-database-id98.fasta
# --ref /usr/local/genome/src/sortmerna-2.0/rRNA_databases/rfam-5.8s-database-id98.fasta
# --ref /usr/local/genome/src/sortmerna-2.0/rRNA_databases/silva-arc-16s-id95.fasta
# --ref /usr/local/genome/src/sortmerna-2.0/rRNA_databases/silva-arc-23s-id98.fasta
# --ref /usr/local/genome/src/sortmerna-2.0/rRNA_databases/silva-bac-16s-id90.fasta
# --ref /usr/local/genome/src/sortmerna-2.0/rRNA_databases/silva-bac-23s-id98.fasta
# --ref /usr/local/genome/src/sortmerna-2.0/rRNA_databases/silva-euk-18s-id95.fasta
# --ref /usr/local/genome/src/sortmerna-2.0/rRNA_databases/silva-euk-28s-id98.fasta
# --reads 3_FASTP/SRR8073716_R1.fastq.gz --reads 3_FASTP/SRR8073716_R2.fastq.gz 
# --workdir 4_SORTMERNA/
--aligned 4_SORTMERNA/SRR8073716-rRNA
--other 4_SORTMERNA/SRR8073716-mRNA
--paired_in
--fastx 
-num_alignments 1
--threads 8 
-v 
&& conda deactivate "
```

### Digital-Normalisation

Pour faciliter l'assemblage, on écarte les reads avec des k-mers trop redondants. 

==`-q highmem.q` ?==

```sh
mkdir 5_KHMER/
qsub -cwd -V -N interleave -o log/ -e log/ -b y "conda activate khmer-3.0.0a3 && interleave-reads.py --gzip 3_FASTP/SRR8073716_R1.fastq.gz 3_FASTP/SRR8073716_R2.fastq.gz -o 5_KHMER/SRR8073716_R1R2.fastq.gz && conda deactivate"
qsub -cwd -V -N khmer -pe thread 8 -q highmem.q -R y -o log/ -e log/ -b y "conda activate khmer-3.0.0a3 && normalize-by-median.py --max-memory-usage 300e9 --paired --gzip --report 5_KHMER/report.txt --output 5_KHMER/SRR8073716_norm.fastq.gz 5_KHMER/SRR8073716_R1R2.fastq.gz && conda deactivate"
```

<!--8h-->  

# Assemblage

Il existe de nombreux outils dédiés à l'assemblage. Tous ne sont pas conçus pour l'assemblage de gros jeux de donnéés très divers comme les métagénomes. En voici deux possédant une option dédiée à l'assemblage de métagénomes. SPADES est bien plus gourmant en mémoire que MEGAHIT mais ils produits un peu moins de chimères et ses contigs sont un peu plus long. 

## avec `metaSPADES`

```sh
mkdir 6_SPADES/
qsub -cwd -V -N spades -pe thread 20 -l h_vmem=10G -R y -o log/ -e log/ -b y "conda activate spades-3.14.0 && spades.py --threads 20 --memory 200 --tmp-dir /projet/tmp --meta -1 3_FASTP/SRR8073716_R1.fastq.gz -2 3_FASTP/SRR8073716_R2.fastq.gz -o 6_SPADES_all/ && conda deactivate"
qsub -cwd -V -N spades -pe thread 20 -l h_vmem=10G -R y -o log/ -e log/ -b y "conda activate spades-3.14.0 && spades.py --threads 20 --memory 200 --tmp-dir /projet/tmp --meta --12 5_KHMER/SRR8073716_norm.fastq.gz -o 6_SPADES/ && conda deactivate"
```

<!--7h30 pour all ou 2h sinon-->  

## avec `MEGAHIT`

```sh
qsub -cwd -V -N megahit -pe thread 20 -l h_vmem=10G -R y -o log/ -e log/ -b y "conda activate megahit-1.2.9 && megahit --12 5_KHMER/SRR8073716_R1R2.fastq.gz --preset meta-large --num-cpu-threads 20 --memory 200 --tmp-dir /projet/tmp --out-dir 7_MEGAHIT/ && conda deactivate "
```

<!--1h30-->  

# Validation de l'assemblage

## Métriques des contigs

Commençons par visualiser les métriques, indépendament des références : Nombre de contigs, Taille totale, N50, L50, ...
Nous pouvons également regarder, par référence, les metriques associées. Notamment le nombre de contigs alignés sur une reference, le pourcentage de génome aligné et le ratio de duplication.
Dzans notre cas (communauté synthétique), cela nous donne une bonne idée de l'efficacité de ces assembleurs.

```sh
mkdir 8_QUAST/
qsub -cwd -V -N quast -pe thread 8 -o log/ -e log/ -b y "conda activate quast-5.0.2 && quast --threads 8 --mgm --output-dir 8_QUAST/ --labels \"spades, megahit\" --pe1 3_FASTP/SRR8073716_R1.fastq.gz --pe2 3_FASTP/SRR8073716_R2.fastq.gz 6_SPADES/contigs.fasta 7_MEGAHIT/final.contigs.fa && conda deactivate"
firefox --no-remote 8_QUAST/report.html

qsub -cwd -V -N quast -pe thread 8 -o log/ -e log/ -b y "conda activate quast-5.0.2 && quast --threads 8 --mgm --output-dir 8_QUAST_2/ --labels \"spades, megahit, spades_all\" --pe1 3_FASTP/SRR8073716_R1.fastq.gz --pe2 3_FASTP/SRR8073716_R2.fastq.gz 6_SPADES/contigs.fasta 7_MEGAHIT/final.contigs.fa 6_SPADES_all/contigs.fasta && conda deactivate"
```

## Binning des contigs

L'assemblage de *shorts reads* issus de métagénomes shotguns permet rarement de reconstruire des génomes complets. Cependant le regroupement de contigs par binning permet de s'approcher et sont un bon compromis aux génomes complets. En effet, bien que fragmentés, ces ébauches de génomes sont souvent dérivées d'espèces individuelles ou de "génomes de population" représentant des séquences consensuelles de différentes souches.

L'approche de binning qu'utilise `MetaBAT ` (*Metagenome Binning with Abundance and Tetra-nucleotide frequencies*) est basé sur la fréquence des tétranucléotides les probabilités d'abondance des contigs pour produit des bins de génomes de haute qualité.

![](https://dfzljdn9uc3pi.cloudfront.net/2015/1165/1/fig-1-2x.jpg)

Bien que `MetaBAT` soit plus adapté à un nombre d'échantillons nombreux et varié, il est possible de l'utiliser sur un unique échantillon ou même uniquement sur la distribution en 4-mer des contigs (c'est à dire, contigs FASTA sans alignement BAM).

```sh
mkdir 9_METABAT/

qsub -cwd -V -N metabat -pe thread 8 -b y "conda activate metabat2-2.12.1 && jgi_summarize_bam_contig_depths --outputDepth 9_METABAT/SRR8073716.depth.txt --minContigLength 1000 --minContigDepth 1 12_BOWTIE/SRR8073716_sorted.bam && metabat2 --numThreads 8 --verbose --unbinned --inFile 6_SPADES/contigs.fasta --outFile 9_METABAT/SRR8073716 --abdFile 9_METABAT/SRR8073716.depth.txt && conda deactivate"
```

Note : il est possible de lancer sans paramètres cette suite de commande avec `runMetaBat.sh <options> assembly.fasta sample1.bam [sample2.bam ...]` . 

MetaBAT sans abundance :

````sh
qsub -cwd -V -N metabat -pe thread 8 -o log/ -e log/ -b y "conda activate metabat2-2.12.1 && metabat2 --inFile 6_SPADES/contigs.fasta --outFile 9_METABAT/SRR8073716 --numThreads 8 --unbinned --verbose && conda deactivate"
````

> Ref : https://bitbucket.org/berkeleylab/metabat/src/master/
>
> Publi : https://doi.org/10.7717/peerj.1165

## Évaluation des bins

Pour l'évaluation des bins, nous utiliserons les deux métriques *completeness* et *contamination* estimés par `CheckM`. Cet outil utilise des sets de gènes marqueurs colocalisés qui sont ubiquitaires et présent en une seule copie au sein d'un taxon phylogénétique donné. Parmi la suite d'outils fourni par `CheckM` nous utiliseront le workflow *lineage-specific* qui est recommandé pour évaluer l'exhaustivité et la contamination des bins de génomes. 

Ce workflow est constitué de 5 étapes :

* Chaque bin de génomes est placé dans l'arbre de génomes de références avec la commande `tree`.
* L'étape suivante `tree_qa` est un contrôle de la qualité de ce placement, basé sur le nombre de marqueurs détecté. Les bins de génomes contenant peut de marqueurs peuvent être écarté si nécessaire.
* La commande `lineage_set` crée un fichier listant les sets de marqueurs spécifique à la lignée à évaluer.
*  Ce fichier marqueur est utilisé par la commande `analyze` afin d'identifier les gènes marqueurs et d'estimer la complétude (*completeness*) et la contamination de chaque bins de génomes. 
* Enfin, la commande `qa` produit différents tableaux résumant la qualité de chaque bins de génomes.

<!-- ![](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC4484387/bin/1043f02.jpg)-->

```sh
mkdir 10_CHECKM/
qsub -cwd -V -N checkm -pe thread 8 -o log/ -e log/ -b y "conda activate checkm-genome-1.0.18 && checkm lineage_wf --extension fa --threads 8 --file 10_CHECKM/report.tsv --tab_table 9_METABAT/ 10_CHECKM/ && conda deactivate"
```

> Source : https://github.com/Ecogenomics/CheckM/wiki
>
> Publi : https://doi.org/10.1101/gr.186072.114

<!--15min-->  

# Annotation

A partir de contigs au format FASTA, `prokka` permet d'annoter rapidement les séquences bactériennes, archéennes et virales (ainsi que les métagénomes avec l'option `--metagenome`). L'utilisation de `prokka` est basé sur une suite d'outils : `prodigal` (détéction des gènes), `RNAmmer` (rRNA), `aragorn` (tRNA), `signalP` (Signal leader peptides), `infernal` (Non-coding RNA).
Au dela de l'annotation syntaxique (position de sgènes), réalisés par Prodigal à partir de modèles HMM précalculés, Prokka assigne pour chaque gène codant pour une protéine une fonction. Pour cela il aligne d'abord compte une banque de protéines bacteriennes annotées manuellement issues de Swissprot, pour les protéines n'ayant pas de similarité suiffisante ( >= 1e)6), il réalise alors une recherche de domaines et motifs contre la baque Pfam.
ces deux approches permettent d'annoter de façon homogène et *très*rapide les familles de gènes conservées. Il est également possible de fournir à prokka un proteome (sous la forme d'un multi-fasta) déjà annoté qu'il utilisera comme banque de referebce pour l'annotation fonctionelle.

```sh
qsub -cwd -V -N prokka -pe thread 8 -o log/ -e log/ -b y "conda activate prokka-1.14.5 && prokka --outdir 11_PROKKA --kingdom Bacteria --metagenome --rnammer --cpus 8 --compliant 6_SPADES/contigs.fasta && conda deactivate"
```

Les résultats de `prokka` sont nombreux et dans des formats facilement exploitables. On retiendra en particulier :

* `.gbk` : Génomes annotés au format Genbank (génomes + annotations)
* `.gff` : Annotations au format GFF3
* `.fna` : Input contigs
* `.ffn` : Régions codantes prédites (nucléique)
* `.faa` : Traduction des régions codantes prédites (protéique)
* `.txt` : Statistiques
* `.log` : Fichier log de l'exécution de prokka

> Source : https://github.com/tseemann/prokka
>
> Publi : https://doi.org/10.1093/bioinformatics/btu153

<!--45min-->  

# Mapping et comptage

```sh
mkdir 12_BOWTIE/
mkdir 12_BOWTIE/index/
qsub -cwd -V -N index -pe thread 8 -o log/ -e log/ -b y "conda activate bowtie2-2.4.1 && bowtie2-build --threads 8 6_SPADES/contigs.fasta 12_BOWTIE/index/SRR8073716 && conda deactivate"

qsub -cwd -V -N bowtie -pe thread 8 -o log/ -e log/ -b y "conda activate bowtie2-2.4.1 && bowtie2 --threads 8 -x 12_BOWTIE/index/SRR8073716 -1 3_FASTP/SRR8073716_R1.fastq.gz -2 3_FASTP/SRR8073716_R2.fastq.gz -S 12_BOWTIE/SRR8073716.sam && conda deactivate"

qsub -cwd -V -N samtools -pe thread 8 -o log/ -e log/ -b y "conda activate samtools-1.9 && samtools sort --threads 8 -o 12_BOWTIE/SRR8073716_sorted.bam 12_BOWTIE/SRR8073716.sam && samtools index 12_BOWTIE/SRR8073716_sorted.bam && conda deactivate"

rm 12_BOWTIE/SRR8073716.sam 
```

## Comptage par contig

```sh
qsub -cwd -V -N idxstats -o log/ -e log/ -b y "conda activate samtools-1.9 && samtools idxstats 12_BOWTIE/SRR8073716_sorted.bam > 12_BOWTIE/contigs_count.tsv && conda deactivate"
```

## Couverture

```sh
qsub -cwd -V -N coverage -o log/ -e log/ -b y "conda activate samtools-1.10 && samtools coverage --output 12_BOWTIE/coverage.tsv 12_BOWTIE/SRR8073716_sorted.bam && conda deactivate"
```

## Comptage par gènes

==# `Overlap between reads and features` à discuter== 

```sh
#qsub -cwd -V -N featureCounts -pe thread 8 -o log/ -e log/ -b y "conda activate subread-2.0.0 && featureCounts -p -B -P -C -O -M --fraction -a 11_PROKKA/PROKKA_07282020.gff -o 12_BOWTIE/genes_count.tsv -t CDS -g ID 12_BOWTIE/SRR8073716_sorted.bam -T 8 && conda deactivate"

# https://github.com/EnvGen/metagenomics-workshop/blob/master/in-house/prokkagff2gtf.sh
grep -v "#" 11_PROKKA/PROKKA_07282020.gff | grep "ID=" | cut -f1 -d ';' | sed 's/ID=//g' | cut -f1,4,5,7,9 |  awk -v OFS='\t' '{print $1,"PROKKA","CDS",$2,$3,".",$4,".","gene_id " $5}' > 11_PROKKA/PROKKA_07282020.gtf

qsub -cwd -V -N htseq -pe thread 8 -o log/ -e log/ -b y "conda activate htseq-0.12.4 && htseq-count --format bam --stranded no --minaqual 10 --mode union --nonunique none --order pos 12_BOWTIE/SRR8073716_sorted.bam 11_PROKKA/PROKKA_07282020.gtf > 12_BOWTIE/genes_count.tsv && conda deactivate"
```

# Cluster des gènes redondants

Pour limiter la redondance et regrouper les gènes proches, on utilise `cd-hit-est`. 

```sh
mkdir 12_CDHIT/
qsub -cwd -V -N cdhit -pe thread 8 -o log/ -e log/ -b y "conda activate cd-hit-4.8.1 && cd-hit-est -i 11_PROKKA/PROKKA_07282020.ffn -o 12_CDHIT/SRR8073716 -c 0.95 -aS 0.90 -d 0 -M 0 -T 8 && conda deactivate"
```

> Source : https://github.com/weizhongli/cdhit
>
> Publi : https://doi.org/10.1093/bioinformatics/bts565

 ## Annotation fonctionnelle des représentants de chaque clusters

```
mkdir 13_EGGNOG/

```



