---
title: "Shotgun metagenomics"
author: "Valentin Loux - Cédric Midoux - Olivier Rué"
subtitle: "Migale facility"
css: css/styles.css
date: "2020/09/15"
output:
    #html_document:
    #  self_contained: true
    #  number_sections: TRUE
    #  code_folding: "show"
    #  toc: true
    #  toc_depth: 2
    #  toc_float: true
    xaringan::moon_reader:
      nature:
        ratio: '16:9'
---



<style type="text/css">
.remark-slide-content {
    font-size: 28px;
}
</style>




```{r setup, include=FALSE}
options(htmltools.dir.version = FALSE)
library(RefManageR)
BibOptions(check.entries = FALSE, bib.style = "authoryear", style = "markdown",
           dashed = TRUE)
file.name <- "biblio.bib"
bib <- ReadBib(file.name)
```



# Practical informations


- 9h00 - 17h00

- 2 breaks morning and afternoon

- Lunch at INRAE restaurant (not mandatory)

- Wearing the mask is mandatory!

---

# Better know us

* Who are you?
  - Institution, laboratory, position …
* What are your needs in metagenomics?
* Do you have already dealed with metagenomic data?
  - Which kind of data?
  - Aim of the study?
* Do you have generated data for a new analysis?
  - Which design? How many samples? Sequencing technology?

---

# Migale team

```{r, out.width = "50%", echo=FALSE, fig.align="center"}
knitr::include_graphics("images/migale-orange.png")
```

* <a href="https://migale.inrae.fr/">Migale website</a>
* Dedicated service to Data Analysis
  - Specialists in Metagenomics
  - Bioinformatics & Statistics
  - More than 60 projects since 2016
  - Collaboration or Accompaniement


---

```{r, out.width = "80%", echo=FALSE, fig.align="center"}
knitr::include_graphics("images/frogs_stuff.gif")
```

---

# Objectives

After this 2 days training, you will:

* Know advantages and limits of shotgun metagenomic data analyses
* Identify tools to analyze your data
* Run tools with Migale resources

---

# Program

.pull-left[
Day 1

* Introduction
* Reminders
* QC
* Taxonomic classification

]

.pull-right[
Day 2

* Cleaning
* Assembly
* Annotation
]

---
class: heading-slide, middle, center

# Introduction to metagenomics analyses

---
## Introduction

The term *Metagenomics* sometimes include:

* Marker-gene sequencing (metataxonomics)
* Shotgun metagenome sequencing
* Meta-transcriptome sequencing

---
## Introduction

The term *Metagenomics* sometimes include:

* Marker-gene sequencing (metataxonomics)
* **Shotgun metagenome sequencing**
* Meta-transcriptome sequencing

---
## Introduction

```{r, fig.align="center", out.width="80%", echo=FALSE}
knitr::include_graphics("images/c6mb00217j-f1_hi-res.gif")
```
---
## Introduction

```{r, fig.align="center", out.width="90%", echo=FALSE}
knitr::include_graphics("images/terminology.png")
```

<cite style="font-size: 0.7em;position: absolute;bottom: 5px;"><a href="https://doi.org/10.1093/bib/bbx120">Breitwieser, Lu and Salzberg (2019)</a></cite>

---
## Introduction

```{r, fig.align="center", out.width="100%", echo=FALSE}
knitr::include_graphics("images/fgene-06-00348-g001.jpg")
```

<cite style="font-size: 0.7em;position: absolute;bottom: 5px;"><a href="https://doi.org/10.1093/bib/bbx120">The Road to Metagenomics, Escobar-Zepeda et al., 2015</a></cite>

---
## Challenges

* Complexity of ecosystem
* Completeness of databases
* Sequencing depth
* Computational resources required


---
## Common analysis procedures for metagenomics data

```{r, fig.align="center", out.width="70%", echo=FALSE}
knitr::include_graphics("images/metagenomics.png")
```

<cite style="font-size: 0.7em;position: absolute;bottom: 5px;"><a href="https://doi.org/10.1093/bib/bbx120">Breitwieser, Lu and Salzberg (2019)</a></cite>
---
class: heading-slide, middle, center
# Reminders

---

## Reminders about Migale facility

* working directories
* SGE submission system
* conda environments

---
## Working directories

* https://tutorials.migale.inra.fr/posts/migale/

---

## Databanks

* https://migale.inrae.fr/databanks
* Shared resources
* Ready-to use
* To ask a new resource: https://migale.inrae.fr/ask-databank

---
## Tools

* Conda environments
* Need to be activated to access binaries
* To ask a new tool: https://migale.inrae.fr/ask-tool

---
class: tp, middle, center

# Switch to TP
---

## Reminders about sequencing

```{r, fig.align="center", out.width="60%", echo=FALSE}
knitr::include_graphics("images/illumina_sequencing.png")
```

---
## Sequencing  - Vocabulary

.pull-left[
**Read** :  piece of sequenced DNA

**DNA fragment** = 1 or more reads depending on whether the sequencing is single end or paird-end

**Insert** = Fragment size

**Depth** = $N*L/G$  
N = number of reads,  
L = size,  
G = genome size

**Coverage** = % of genome covered
]
.pull-right[
```{r, fig.align="center", out.width="80%",  echo=FALSE}
knitr::include_graphics("images/se-pe.png")
```

```{r, fig.align="center", out.width="80%", , echo=FALSE}
knitr::include_graphics("images/fragment-insert.png")
```

```{r, fig.align="center", out.width="80%", fig.cap= "Single-End , Paired-End" , echo=FALSE}
knitr::include_graphics("images/depth-breadth.png")
```

]

---
## Sequencing data

* Huge amount of reads (up to billions)
* FASTQ format

```{r, fig.align="center", out.width="80%", , echo=FALSE}
# knitr::include_graphics("images/sequencing.png")
```

---
class: heading-slide, middle, center
# FASTQ format

---

## FASTQ syntax

The FASTQ format is the de facto standard by which all sequencing instruments represent
data. It may be thought of as a variant of the FASTA format that allows it to associate a
quality measure to each sequence base:   **FASTA with QUALITIES**.

---

## FASTQ syntax

The FASTQ format consists of 4 sections:
1. A FASTA-like header, but instead of the <code>></code> symbol it uses the <code>@</code> symbol. This is followed
by an ID and more optional text, similar to the FASTA headers.
2. The second section contains the measured sequence (typically on a single line), but it
may be wrapped until the <code>+</code> sign starts the next section.
3. The third section is marked by the <code>+</code> sign and may be optionally followed by the same
sequence id and header as the first section
4. The last line encodes the quality values for the sequence in section 2, and must be of
the same length as section 2.

---

## FASTQ syntax

<i>Example</i>

```{bash, eval=FALSE}
@SEQ_ID
GATTTGGGGTTCAAAGCAGTATCGATCAAATAGTAAATCCATTTGTTCAACTCACAGTTT
+
!''*((((***+))%%%++)(%%%%).1***-+*''))**55CCF>>>>>>CCCCCCC65
```

---

## FASTQ quality

Each character represents a numerical value: a so-called Phred score, encoded via a single letter encoding.

```{bash, eval=FALSE}
!"#$%&'()*+,-./0123456789:;<=>?@ABCDEFGHI
|    |    |    |    |    |    |    |    |
0....5...10...15...20...25...30...35...40
|    |    |    |    |    |    |    |    |
worst................................best
```

The numbers represent the error probabilities  via the formula: $Error=10^{-P/10}$

It is basically summarized as:

- P=0 means 1/1 (100% probability of error)
- P=10 means 1/10 (10% probability of error)
- P=20 means 1/100 (1% probability of error)
- P=30 means 1/1000 (0.1% probability of error)
- P=40 means 1/10000 (0.01% probability of error)

---

## FASTQ quality encoding specificities

There was a time when instrumentation makers could not decide at what
character to start the scale. The **current standard** shown above is the so-called Sanger (+33)
format where the ASCII codes are shifted by 33. There is the so-called +64 format that
starts close to where the other scale ends.


```{r, fig.align="center", out.width="80%", fig.cap= "FASTQ encoding values" , echo=FALSE}
knitr::include_graphics("images/qualityscore.png")
```

---

## FASTQ Header informations

Information is often encoded in the “free” text section of a FASTQ file.

<code>@EAS139:136:FC706VJ:2:2104:15343:197393 1:Y:18:ATCACG</code> contains the following information:

- <code>EAS139</code>: the unique instrument name
- <code>136</code>: the run id
- <code>FC706VJ</code>: the flowcell id
- <code>2</code>: flowcell lane
- <code>2104</code>: tile number within the flowcell lane
- <code>15343</code>: ‘x’-coordinate of the cluster within the tile
- <code>197393</code>: ‘y’-coordinate of the cluster within the tile
- <code>1</code>: the member of a pair, 1 or 2 (paired-end or mate-pair reads only)
- <code>Y</code>: Y if the read is filtered, N otherwise
- <code>18</code>: 0 when none of the control bits are on, otherwise it is an even number
- <code>ATCACG</code>: index sequence

This information is specific to a particular instrument/vendor and may change with different
versions or releases of that instrument.

---
class: heading-slide, middle, center
# Quality control
---

## Why QC'ing your reads ?

Try to answer to (not always) simple questions:
--

- Are data conform to the expected level of performance?
  - Size
  - Number of reads
  - Quality
- Residual presence of adapters or indexes ?
- Are there (un)expected techincal biases
- Arte ther (un)expected biological biases

<div class="alert comment">`r icon::fa("exclamation-circle")` Quality control without context leads to misinterpretation</div>

---
## Quality control for FASTQ files

- FastQC `r Citep(bib, "fastqc")`
  - QC for (Illumina) FastQ files
  - Command line fastqc or graphical interface
  - Complete HTML report to spot problem originating from sequencer, library preparation, contamination
  - Summary graphs and tables to quickly assess your data

  
```{r, fig.align="center", out.width="40%", echo=FALSE}
knitr::include_graphics("images/fastqc.png")
```

- https://rtsf.natsci.msu.edu/genomics/tech-notes/fastqc-tutorial-and-faq/

---
class: tp, middle, center

# Switch to TP
---

class: heading-slide, middle, center
# Taxonomic affiliation

---
## Strategies

* Assign a taxon identifier to each read
* Methods
  * Alignment
  * Mapping
  * K-mer matches
* Level
  * Nucleic
  * Proteic
* Relies on taxonomic information

---
## Kraken

* First method

```{r, out.width="70%", fig.align="center", echo=FALSE}
knitr::include_graphics("images/kraken.png")
```

<cite style="font-size: 0.7em;position: absolute;bottom: 5px;"><a href="https://doi.org/10.1186/gb-2014-15-3-r46">Wood and Salzberg (2014)</a></cite>

---
## Kraken

* Very fast
* Database build may be long and need a lot of memory

---
## Kaiju

* Database of proteic sequences
* Supposed to be more sensitive
* Translate reads in all six reading frames, split at stop codons
* Use BWT and FM-index table

<cite style="font-size: 0.7em;position: absolute;bottom: 5px;"><a href="https://doi.org/10.1038/ncomms11257">Menzel et al. (2016)</a></cite>

---
## Kaiju databanks


```{r, fig.align="center", out.width="45%", echo=FALSE}
knitr::include_graphics("images/kaiju_databanks.png")
```


---
## Taxonomic classification caveats

* Databanks
* K-mer choice (sensitivity / specificity)
* Allow a "fast" overview of your data
  - Contaminants?
  - Host reads?
  - Classification rate

<cite style="font-size: 0.7em;position: absolute;bottom: 5px;"><a href="https://doi.org/10.1093/bib/bbx120">Breitwieser et al. (2019)</a></cite>

---
class: tp, middle, center

# Switch to TP
---
class: heading-slide, middle, center
# Reads cleaning
---

## Objectives

- Detect and remove sequencing adapters (still) present in the FastQ files
- Filter / trim reads according to quality (as plotted in FastQC)

## Tools

- Simple & fast : Sickle `r Citep(bib, "sickle")` (quality), cutadapt `r Citep(bib, "cutadapt")` (adpater removal)
- Ultra-configurable : Trimmomatic `r Citep(bib, "trimmomatic")`
- All in one & ultra-fast : fastp `r Citep(bib, "fastp")`

```{r, fig.align="center", out.width="45%", echo=FALSE}
knitr::include_graphics("images/fastp_wkwf.png")
```

---
# rRNA filering

- Essantial step in metatranscriptomic, can be applied in metagenomic

## Tool

SortMeRNA `r Citep(bib, "sortmerna")`
- SortMeRNA takes as input :
  - one or two (paired) reads file(s)
  - one or multiple rRNA database file(s) with index
- Sorts apart aligned and rejected reads into two files. 

---
class: tp, middle, center

# Switch to TP
---
class: heading-slide, middle, center
# Alignment

---
# Alignment strategies

```{bash, eval=FALSE}
GAAGCTCTAGGATTACGATCTTGATCGCCGGGAAATTATGATCCTGACCTGAGTTTAAGGCATGGACCCATAA
                 ATCTTGATCGCCGAC----ATT       # GLOBAL
                 ATCTTGATCGCCGACATT           # LOCAL, with soft clipping
```

## Global alignment

Global alignments, which attempt to align every residue in every sequence, are most useful when the sequences in the query set are similar and of roughly equal size. (This does not mean global alignments cannot start and/or end in gaps.) A general global alignment technique is the <code>Needleman–Wunsch algorithm</code>, which is based on dynamic programming.

---
# Alignment strategies

```{bash, eval=FALSE}
GAAGCTCTAGGATTACGATCTTGATCGCCGGGAAATTATGATCCTGACCTGAGTTTAAGGCATGGACCCATAA
                 ATCTTGATCGCCGAC----ATT       # GLOBAL
                 ATCTTGATCGCCGACATT           # LOCAL, with soft clipping
```

## Local alignment

Local alignments are more useful for dissimilar sequences that are suspected to contain regions of similarity or similar sequence motifs within their larger sequence context. The <code>Smith–Waterman algorithm</code> is a general local alignment method based on the same dynamic programming scheme but with additional choices to start and end at any place.

---
# Seed-and-extend especially adapted to NGS data


.pull-left[
Seed-and-extend mappers are a class of read mappers that break down each read sequence into seeds (i.e., smaller segments) to ﬁnd locations in the reference genome that closely match the read.

```{r, fig.align="center", out.width="100%", fig.cap= "" , echo=FALSE}
knitr::include_graphics("images/seed_and_extend.png")
```
]

.pull-right[
1. The mapper obtains a read
2. The mapper selects smaller DNA segments from the read to
serve as seeds
3. The mapper indexes a data structure with each seed to
obtain a list of possible locations within the reference genome that could result in
a match
4. For each possible location in the list, the mapper obtains the
corresponding DNA sequence from the reference genome
5. The mapper aligns the read sequence to the reference sequence, using an expensive sequence
alignment (i.e., veriﬁcation) algorithm to determine the similarity between the read
sequence and the reference sequence.
]


---
# Mapping tools

```{r, fig.align="center", out.width="100%", fig.cap="" , echo=FALSE}
knitr::include_graphics("images/mapping_tools.png")
```

---
class: tp, middle, center

# Switch to TP
---
class: heading-slide, middle, center
# Metagenomic Assembly
---

## Objectives

- Reconstruct genes and organisms from complex mixtures
- Dealing with the ecosystem's heterogeneity, multiple genomes at varying levels of abundance
- Limiting the reconstruction of chimeras

```{r, out.width="70%", fig.align="center", echo=FALSE}
knitr::include_graphics("https://oup.silverchair-cdn.com/oup/backfile/Content_public/Journal/bib/21/2/10.1093_bib_bbz020/4/bbz020f1.png?Expires=1602505534&Signature=A0Z~5RHAP9RrEMU7HI9xqobe5H015WtC0M63JisWd-Be5HI8hLMF~xscpsAt644Bp1HTMdq28~cam5k83svkPnhtfNumjf7FSSfKyGmwympn9tbzPMzskmKbB9TkNTabHqb~qxesSQXAXyWMRdypXbB5y07ez4uZ9Fi3T2SZtFO4CmOJ0Zw2z~1c7xmqwb-gQOpedMZPEqME9~y-xmEaKMW-9xLwY1bQI6SA3t1gRysJxm1T0Hl-LUQ~mQLZZkKK7RMbEbB~4~J9f2RDkhMUcXG0eQWqvupt2DajlDBLsYyyOIazNMzlmrkcSfOxcgBLyDKTXF9fmEtfDiioI8wY~g__&Key-Pair-Id=APKAIE5G5CRDK6RD3PGA")
```
---
## Tools

- Generic tool with a meta option : SPAdes and metaSPAdes `r Citep(bib, "spades")`
- Tools requiring less memory : MEGAHIT `r Citep(bib, "megahit")`
- The historical tool allowing many parameters : Velvet (and MetaVelvet) [Only for training !]
- A (not so) recent benchmark of short reads metagenome assemblers.  `r Citep(bib, "Vollmers2017")`
- Long read / Hybrid assemblies use different algorithms and strategies and are still a  research question.

---
class: tp, middle, center

# Switch to TP
---

## Assessment of assembly quality

After assembly, we use MetaQUAST `r Citep(bib, "metaquast")` to evaluate and compare metagenome assemblies.

What MetQUAST does :
- De novo metagenomic assembly evaluation
- [Optionally] identify reference genomes from the content of the assembly 
- Reference-based evaluation
- Filtering so-called misassemblies based on read mapping 
- Report and visulaisation

---
## De novo metrics 
Evaluation of the assembly based on 
* Number of contigs greater than a given threshold (0, 500nct, 1kb)
* Total / thresholded assemby size
* largest contig size
* N50 : the sequence length of the shortest contig at 50% of the total assembly length (equivalent to a median of contig lengths)
* L50 : the number of contigs  at 50% of the total assembly length
* N75/L75 idem, for 75% of the assembly length

---
## Reference-based metrics

* Metrics based on the comparison with reference genomes.

* Reference genomes are given by the user or automatically constitued by MetaQuast based on comparison of rRNA genes content of the assembly and a reference database (Silva). Complete genomes are then automatically donloaded.

* For each given reference genome, based on an alignement of all contigs on it :
  - duplication rate 
  - percent genome complete
  - NGA50 : equivalent of N50 but with the aligned block of the contigs
  - "Misassemblies" : breakpoint of alignement in a contigs. "false misassemblies" (i.e. true inversion/translocation/…) are filterd by alligning paired-reads on reference. If a "misassembly" is supported by paired-end reads happily aligned, it is counted.
  - An individula Quast report and an alignement visualisation.

---
class: tp, middle, center

# Switch to TP
---
class: heading-slide, middle, center
# Mapping
---
# Mapping 

* We used only a portion of the reads for assembly. 
* For further analysis it is necessary to map all the reads on the contigs.

```{r, fig.align="center", out.width="55%", fig.cap="" , echo=FALSE}
knitr::include_graphics("images/mapping_tools.png")
```

* We will use BWA `r Citep(bib, "bwa")`
  * Firstly, we build an index.
  * Secondly, reads are aligned.
  * We can use samtools `r Citep(bib, "samtools")` and bedtools `r Citep(bib, "bedtools")` to manipulate SAM/BAM files.

---
class: tp, middle, center

# Switch to TP
---
class: heading-slide, middle, center
# Contig Binning
---

## Objectives

- Binning is a good compromise when the assembly of whole genomes is not feasible.
- Similar contigs are grouped together.

```{r, out.width="60%", fig.align="center", echo=FALSE}
knitr::include_graphics("https://www.ncbi.nlm.nih.gov/pmc/articles/PMC5045144/bin/yjbm_89_3_353_g02.jpg")
```

---
## Approch

- MetaBAT `r Citep(bib, "metabat")` is a tool for reconstructing genomes from complex microbial communities. 
- Binning approch is based :
  - Tetranucleotide frequency
  - Abundance (i.e., mean base coverage)

```{r, out.width="50%", fig.align="center", fig.cap="metabat", echo=FALSE}
knitr::include_graphics("https://dfzljdn9uc3pi.cloudfront.net/2015/1165/1/fig-1-2x.jpg")
```

---
## Bins evaluation

- For the evaluation of bins, we will use *completeness* and *contamination* estimated by CheckM `r Citep(bib, "checkm")`. 
- Use of collocated sets of genes that are ubiquitous and single-copy within a phylogenetic lineage.
- Among a set of tools in CheckM we will use the `checkm lineage_wf` workflow which only mandatory requires a directory of genome bins.


--

CheckM is included in the M-Tools suite (https://ecogenomic.org/m-tools) with RefineM (population genomes), GraftM (marker genes), GroopM (metagenomic binning), BamM (parse BAM files), FinishM (improve/finish a draft genome), CoverM (read coverage calculator), ...

---
class: tp, middle, center

# Switch to TP
---
class: heading-slide, middle, center

# Annotation
---
# Annotation

* Goal : 
  - Syntaxic annotation (gene prediction)
  - Functionnal annotation (function prediction for protein coding genes)
  
* Prokka `r Citep(bib, "prokka")` is a is a software tool to annotate bacterial, archaeal and viral genomes **quickly** and produce standards-compliant output files.
* Prokka *automatically* annotate a complete bacterial genome in ~5mn.  
* Prokka will not replace expert annotation but gives you an homogenoeus procedure for annotation of conserved genes familly

---

## Genes prediction

- Gene prediction in complete prokaryotic geneomes isn't as such a problem.
- Efficient gene predictors are available (bactgeneSHOW, prodigal, glimmerHMM,…).
- Most of them uses HMM models to predict the gene structure

- Gene prediction on metagnomes is difficult due to :
  - assembly fragmentation
  - assmbly errors, frameshift, chimeras,…
  - different species in teh same sample that could/shoudl lead to use different models
  
- Prodigal (withe -meta parameter) and fraggenescan have good enough results on metagenomic contigs.

- Caution to partial genes !

---
# Prokka pipeline

- Coding gene prediction with Prodigal `r Citep(bib, "prodigal")`
- tRna; rRna gene prediction with Aragorn, Barnap, RNAmmer (optionnal)
- Functionnal annotation based on similarity search with a threshold (1e-6) and hierarchically against :
  - [Optionnal] a given proteome ( `--proteins` parameter)
  - [ISFinder](https://isfinder.biotoul.fr/)  for transposases, not entire IS
  - [NCBI Bacterial Antimicrobial Resistance Reference Gene Database](https://www.ncbi.nlm.nih.gov/bioproject/313047) for Antimicrobial Resistance Genes.
  - [UniprotKB/Swissprot](https://www.uniprot.org/uniprot/?query=reviewed:yes) **curated** proteins with evidence that (i) from Bacteria (or Archaea or Viruses); (ii) not be "Fragment" entries; and (iii) have an evidence level ("PE") of 2 or lower, which corresponds to experimental mRNA or proteomics evidence.
  - Domain and motifs (hmmsearch) :
    - [Pfam](https://pfam.xfam.org)
    - [HAMAP](https://hamap.expasy.org)


---
# Prokka pipeline

```{r, out.width="50%", fig.align="center", fig.cap="metabat", echo=FALSE}
knitr::include_graphics("images/prokka-pipelinepng.png")
```

---
## EggNogg Mapper

* eggNOG-mapper `r Citep(bib, "eggnogmapper2017")` is a tool for fast functional annotation of novel sequences. It uses precomputed orthologous groups and phylogenies from the eggNOG database to transfer functional information from fine-grained orthologs only.

* eggNOG uses hmmsearch to search against HMM eggNOG database OR Diamond to search against eggNOG protein database 

* Refine first hit  using a list of precomputed orthologs. Assign one ortholog

* Functionnal annotation using this ortholog

---
## Other options for functionnal annotation
### Diamond
Diamond `r Citep(bib, "diamond2014")`  is a sequence aligner for protein (equivalent blastp) and translated DNA (equivalent tblastx) searches, designed for high performance analysis of big sequence data.  Diamond is  100x to 20,000x the speed of BLAST.

* Diamond could be used to query against any given databank.

### ghostKOALA `r Citep(bib, "ghostkoala")` [Online]
KOALA (KEGG Orthology And Links Annotation) is KEGG's internal annotation tool for K number assignment. 

### Clustering of protein coding genes :
cd-hit `r Citep(bib, "cdhit2012")` is a software for clustering protein sequences. It can be used to downsize the number of lines in the  in the gene count matrix.
---
class: heading-slide, middle, center

# Automatization
---
# Automatization

.pull-left[
We have developed a workflow that allows us to automate all these analyses.
* developed with snakemake
* executable on MIGALE

```json
{
	"SAMPLES": ["mock"],
	"NORMALIZATION": true,
	"SORTMERNA": true,
	"ASSEMBLER": "metaspades",
	"CONTIGS_LEN": 1000,
	"PROTEINS-PREDICTOR": "prodigal"
}
```

https://gitlab.irstea.fr/cedric.midoux/workflow_metagenomics/

]
.pull-right[
```{r, fig.align="center", out.width="90%",  echo=FALSE}
knitr::include_graphics("images/mock-global_graph.png")
```
]

---
# Autres pistes/outils

* [Pear](https://cme.h-its.org/exelixis/web/software/pear/index.html) : paired-end merger très efficace.


*  SimkaMin: fast and resource frugal de novo comparative metagenomics
`r Citep(bib, "simka2019")` 
A quick comparative metagenomics tool with low disk and memory footprints, thanks to an efficient data subsampling scheme used to estimate Bray-Curtis and Jaccard dissimilarities. 
Très efficace dans le QC pour faire une première comparaison des échantillons / réplicats.

* Cd-hit :  `r Citep(bib, "cdhit2012")` 
clustering efficace des CDS.

* Linclust (MMseqs2) : Clustering huge protein sequence sets in linear time.  `r Citep(bib, "linclust2018")` 
Clustering plus efficace (en temps de calcul), sur des très gros jeux de données que cd-hit.

* PLASS : Protein-Level ASSembler
Assemble short read sequencing data on a protein level. `r Citep(bib, "plass2019")`  The main purpose of Plass is the assembly of complex metagenomic datasets. It assembles 10 times more protein residues in soil metagenomes than Megahit. 

---
# GraftM :  a tool for scalable, phylogenetically informed classification of genes within metagenomes

![](https://www.ncbi.nlm.nih.gov/pmc/articles/instance/6007438/bin/gky174fig1.jpg)
`r Citep(bib, "graftM2018")` 
---

# Anvi’o: integrated multi-omics at scale

* Anvi'o `r Citep(bib, "anvio2015")` is  an open-source, community-driven analysis and visualization platform for microbial ‘omics.
* With [this tutorial](http://merenlab.org/2016/06/22/anvio-tutorial-v2/), starting from a metagenomic assembly, you will :
- Process your contigs,
- Profile your metagenomic samples and merge them,
- Visualize your data, identify and/or refine genome bins interactively, and create summaries of your results.

---
# Metagenomics atlas

* Metagenome-atlas  `r Citep(bib, "metagenomeatlas20")` is an easy-to-use metagenomic pipeline based on *snakemake*. 
* It handles all steps of analysis :
  - QC
  - Assembly
  - Binning
  - Annotation.


---
# Metagenomics and long reads

* Long read data can be used to improve assembly 
* Bottlenecks :
  - DNA extraction (?)
  - cost of data generation
  - sequencing errors 

* State of the art pipeline for assembly :
 - standalone long read assembly (ex:  MetaFLYE `r Citep(bib, "metaFlye19")`)
 - optionnal error correction with short reads
 
---
#Take home message

- Shotgun metagenomics is still an ongoing active bioinformatics research field
- Numerous  software dedicated to assembly, binning, functionnal annotation are actively developed


- Depending on the ecosystem , one can have different approaches :
  - mapping on a reference database
  - assembly and mapping

- Biostatistics…

---
# References
```{r, results='asis', echo=FALSE}
PrintBibliography(bib,start=1,end=5)
```
---

# References(2)
```{r, results='asis', echo=FALSE}
PrintBibliography(bib,start=6,end=10)
```

---
# References(3)
```{r, results='asis', echo=FALSE}
PrintBibliography(bib,start=11,end=15)
```

---
# References(4)
```{r, results='asis', echo=FALSE}
PrintBibliography(bib,start=16,end=20)
```

---
# References(5)
```{r, results='asis', echo=FALSE}
PrintBibliography(bib,start=21,end=25)
```


