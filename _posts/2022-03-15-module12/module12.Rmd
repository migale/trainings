---
title: "Initiation au langage R"
description: |
  À l’issue de la formation, les stagiaires connaîtront les principales fonctionnalités du langage R et ses principes. Ils seront capables de les appliquer pour effectuer des calculs ou des représentations graphiques simples. Ils seront de plus autonomes pour manipuler leurs tableaux de données. Attention : ce module n'est ni un module de statistique, ni un module d'analyse statistique des données.
author:
  - name: Sophie Schbath
    #url: https://example.com/norajones
    affiliation: MaIAGE - Migale - INRAE
    #orcid_id: 0000-0001-7517-4724
  - name: Véronique Martin
    #url: https://example.com/norajones
    #orcid_id: 0000-0002-7964-0929
    affiliation: MaIAGE - Migale - INRAE
date: "2022-03-15"
preview: preview.png
categories:
  - R
  - module 12
  - "2022"
output:
  distill::distill_article:
    self_contained: false
    css: [../../css/theme.css,'https://use.fontawesome.com/releases/v5.0.9/css/all.css']
creative_commons: CC BY-SA
bibliography: ../../resources/biblio.bib
csl: ../../resources/biomed-central.csl
---



```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = FALSE)
```

```{r load_packages, message=FALSE, warning=FALSE, include=FALSE} 
library(fontawesome)
```


```{r, eval=FALSE}
download.file("https://analyses.migale.inra.fr/resources/biblio.bib")
#download.file("https://analyses.migale.inra.fr/resources/biomed-central.csl", "biomed-central.csl")
```



Retrouvez <a href="https://migale.inrae.fr/sites/default/files/formation/2022/module12.pdf">la plaquette</a> de la formation 


Aucun prérequis n'est nécessaire pour cette formation `r fa("exclamation-triangle", fill = "red")`

***

Voici tous les supports présentés lors de cette formation :

* [Slides sans solution](content/2021-slides-initR-module12-sans-solution.pdf)

