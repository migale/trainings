# Planning

## J1

### 9h00 - 9h20

* Présentation formation / participants (tour de table)
* Rappels infra

### 9h20 - 9h40

* MetaG

### 10h - 10h30

* Format de données
* Stockage des données

### 10h30 - 12h00

* QC
* Taxo
* Visu

### 13h30 - 

* Nettoyage (reads + sortmerna)


### Assemblage - Mapping

### Binning + Taxo + CheckM

### Fonctionnel + Annotation + Clustering

### Autres : Simka, assemblage ciblé, GraftM