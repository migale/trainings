---
title: "Bonnes pratiques en bioinformatique : (essayer) d'aller vers plus de reproductibilité"
author: "Cédric Midoux - Valentin Loux - Olivier Rué"
subtitle: "Plateforme Migale"
date: "2022-05-20"
bibliography: ../../../resources/biblio.bib
csl: ../../../resources/biomed-central.csl
output:
    html_document:
      css: [../../../css/html_doc.css, 'https://use.fontawesome.com/releases/v5.0.9/css/all.css']
      self_contained: true
      number_sections: true
      code_folding: "show"
      toc: true
      toc_depth: 3
      toc_float: true
      includes:
        after_body: ../../../resources/footer.html
    
---

# Prérequis

## Créer un compte sur un service web distant

* Créer un compte sur [GitHub](https://github.com/signup)
* Fonctionne également avec GitLab tel que [ForgeMIA](http://forgemia.inra.fr) (pour le personnel INRAE)

## Installer git ainsi qu'une application graphique d'aide au développement

### Installation de Git

* Sur unix, git est installé par défaut.

```{bash, eval=FALSE}
git --version
## git version 2.25.1
```

* Si vous êtes sur Windows, se référer à https://git-for-windows.github.io/

> Suppléments : [Happy git with R - 6: Install git](https://happygitwithr.com/install-git.html)

* Sous MacOS , testez la commande 

```{bash, eval=FALSE}
git --version
## git version 2.32.0 (Apple Git-132)
```

Si git n'est pas installé, une invit vous proposera de l'installer pour vous.

### Installation de GithubDesktop

* Pour commencer, nous utiliserons [GitHub Desktop](https://desktop.github.com/).  
Il existe de nombreux autres outils prenant en charge un gestionnaire de versions, tel-que RStudio, VSCode, GitKraken, Sublime Merge, ...


## Connexion entre la session local et le compte distant (sur github.com)

* Entre GitHub Desktop et github.com, la synergie est maximale. On utilisera les même identifiants pour se connecter des deux cotés.

* Note : sur les sessions `stage` des ordinateurs de formation, il peut être nécessaire de créer un trousseau

### Clés SSH

* Pour interagir avec le dépôt distant sans renseigner ses identifiants à chaque fois, on utilisera une clé ssh (Secure Shell). Elles vont par paires comme par exemple :

  * `id_rsa` et `id_rsa.pub`
  * `id_ecdsa` et `id_ecdsa.pub`
  * `id_ed25519` et `id_ed25519.pub`

Les clés sont généralement stocké dans le dossier `~/.ssh`

* Vérifier si l'on dispose déjà d'une paire de clés (elles sont dans le dossier `~/.ssh`)

```{bash, eval=FALSE}
ls -al ~/.ssh
```

* Si vous n'avez pas de clé existante, générer la paire de clés avec `ssh-keygen` :

```{bash, eval=FALSE}
ssh-keygen -t rsa -b 4096 -C "your_email@example.com"
```

* L'authentification des opérations menées à partir de Git sur GitHub passe exclusivement par l'échange de clefs ssh. Il vous faut donc faire connaitre à GitHub votre clef **publique** en la copiant dans son interface.

* Renseigner la clé publique sur GitHub, via l'interface graphique : https://github.com/settings/keys 

```{bash, eval=FALSE}
cat ~/.ssh/id_rsa.pub
```

* vérifier que tout fonctionne

```{bash, eval=FALSE}
 ssh -T git@github.com
# A la première connexion, vous pouvez voir un warning de ce type :
#> The authenticity of host 'github.com (IP ADDRESS)' can't be established.
#> RSA key fingerprint is SHA256:nThbg6kXUpJWGl7E1IGOCspRomTxdCARLviKw6E5SY8.
#> Are you sure you want to continue connecting (yes/no)?
# Verify that the fingerprint in the message you see matches GitHub's RSA public key fingerprint ( https://docs.github.com/en/github/authenticating-to-github/githubs-ssh-key-fingerprints ). If it does, then type yes:

## > Hi username! You've successfully authenticated, but GitHub does not provide shell access.
```

> [Procédure détaillée Mac, Windows, Linux](https://docs.github.com/en/github/authenticating-to-github/connecting-to-github-with-ssh)

> Pour aller plus loin sur le [chiffrage asymétrique](https://fr.wikipedia.org/wiki/Cryptographie_asym%C3%A9trique)

## Renseigner ces identifiants

Configuration des informations de l'utilisateur pour tous les dépôts locaux

* Via les options de GitHub Desktop

* Si vous utilisez git en ligne de commande 
```{bash, eval=FALSE}
git config --global user.name "[nom]"
git config --global user.email "[email]"
git config --global color.ui auto
```

* Si vous utilisez git avec R
```{r, eval=FALSE}
usethis::use_git_config(user.name = "[nom]", user.email = "[email]")
```

* Pour connaitre la configuration actuelle 
```{bash, eval=FALSE}
git config --list
```

# Versionner ses documents avec git

## Créer un dépôt distant sur GitHub

```{r, out.width = "50%", echo=FALSE, fig.align='center'}
knitr::include_graphics("images/github-newrepo.png")
```


```{r, out.width = "100%", echo=FALSE}
knitr::include_graphics("images/github-create-repo.png")
```

```{r, out.width = "100%", echo=FALSE}
knitr::include_graphics("images/github-create-repo-2.png")
```

Un dépôt est définit avec l'identifiant `<username>/<repository>` (ici `CedricMidoux/migale2022`)

Notez la première ligne avec l'adresse pour récupérer le dépôt en sélectionnant le mode SSH.

Remarque : Bien que cela soit parfois un peu plus délicat, il est aussi possible d'initier un dépôt localement avec `git init` mais dans ce cas il faudra ensuite lier ce dépôt à un remote distant si l'on le souhaite. 

```{bash, eval=FALSE}
mkdir testRepo
cd testRepo
# Initialized empty Git repository in /Users/vloux/tmp/testRepo/.git/
git init
# Add a new local git repo to an existing and empty Github repo : 
git remote add origin git@github.com:vloux/testRepo.git
#Verify new remote
git remote -v
git branch -M main
git push -u origin main
```

## Cloner le dépôt depuis GitHub vers la session locale

* Via l'interface graphique, on sélectionne facilement notre dépôt.

* Sinon on utilise le lien fourni avec le mode SSH. 

* *En ligne de commande, on utilise `git clone`*

```{bash, eval=FALSE}
git clone git@github.com:CedricMidoux/migale2022.git
```

Le dépôt est vide et disponible localement.

## Ajouter un document (en local)

* Ouvrir le dossier dans l'explorateur de fichiers

* Ajouter un fichier `README.md` avec quelques lignes de texte.

* Les modifications sont listées dans la colonne de gauche `Change`.

* On ajoute les modifications a suivre avec les cases a cocher.

* Lorsque l'on est satisfait on peut figer l'état avec un **`commit`**. On renseigne un message de commit explicite et `Commit to main`. 

* *En ligne de commande on utilise `git status` pour connaître l'état de notre dépôt. `git add` pour ajouter un fichier a l'index, puis lorsque toutes les modifications sont suivies, on peut commit avec `git commit`.*

## Modifier un fichier et observer les différences

* On modifie le fichier et observe ces différences dans le pannel

* *En ligne de commande on utilise `git status` pour suivre comparer les versions.*

## Changer de branche

* On travaille actuellement sur la branch principale `main`. On peut créer une nouvelle branche et changer de branche avec l'onglet dédié.

## Pousser les modifications locales sur le dépôt distant

* Pour l'instant les modifications sont juste en local, il est nécessaire de les envoyer vers le remote distant (server GitHub)

* Ici encore, interface graphique nous facilite la tache avec l'onglet `Publish branch`

* *La commande `git push` pousse les modifications de la branche `main` (la branche par défaut locale) sur la branche `origin` (le nom de la branche distante). On peut aussi utiliser la version longue `git push origin master`*

* On peut lister les différents dépôts distants avec 

```{bash, eval=FALSE}
git remote -v
# origin	git@github.com:CedricMidoux/migale2022.git (fetch)
# origin	git@github.com:CedricMidoux/migale2022.git (push)
```

## Verification sur GitHub des modifications du dépôt

* Connectez vous sur l'interface de GitHub :
  * Trouvez votre dépôt
  * Vérifiez que vous trouvez bien le fichier que vous avez modifié, les commits

## Modification du dépôt par l'interface

* Editez `README.md` depuis l'interface en ligne
* Commitez la modification

##  Récupérer sur le repo local des infos de la branche distante

* On récupère localement les modifications distante avec `Pull origin` (ou `git pull`) qu'il s'agisse de modifications faite en ligne ou par un collègue travaillant sur le même projet.

## Le journal des modifications

On peut explorer les commit avec l'onglet `History` (ou `git log`).

```{bash, eval=FALSE}
git log
# commit 4606251a3f1a63874803c1a2397072b7ac60ebf1 (HEAD -> master, origin/master)
# Author: CedricMidoux <34483895+CedricMidoux@users.noreply.github.com>
# Date:   Mon Mar 8 11:30:27 2022 +0100
# 
#     Update README.md
# 
# ...
```

Pour chaque commit on a un identifiant (SHA), l'auteur, la date et le commentaire (d'où l'interet de correctement nommer ses commits)

```{r, out.width = "50%", echo=FALSE, fig.align='center'}
knitr::include_graphics("https://imgs.xkcd.com/comics/git_commit.png")
```


## Naviguer entre les commits

* A partir de l'historique des commit, on peut revenir a une ancienne version avec `Revert changes in commit`

* Si cela entraîne des conflits, il faut les traiter manuellement. On peut s'aider d'interface dédiée tel-que meld.

* Sinon on peut également tirer une branche a partir d'un (ancien) commit pour travailler sur une fonctionnalité dédiée. 

* En ligne de commande, on utilise `git checkout [SHA]` ou `git checkout [branch]`

## Nommer des versions (tags)

* Pour fixer une version, communiquer plus facilement ou annoncer une modification majeure, il est courant d'utiliser des **tags**.

* Sur l'interface en ligne de GitHub, on retrouve les tags.

```{r, out.width = "50%", echo=FALSE, fig.align='center'}
knitr::include_graphics("images/github-tags.png")
```


## Travailler à plusieurs - *Issues*

* Posez une question a votre voisin de formation en utilisant le bouton `New Issue`

## Travailler à plusieurs - *Pull request*

* Via l'interface en ligne, créez une branche `fork` de votre voisin de formation.
* Faites une modification
* Créez une pull request et argumentez votre modification
* Explorez les suggestions que vous avez reçu
* Merge pull

# Documents Computationel  : Rédaction d'un notebook avec RStudio

1. Connectez vous au [RStudio de migale](https://rstudio.migale.inrae.fr).

2. Observer son profil `git config --global`

```{r, eval=FALSE}
usethis::git_sitrep()
# Git config (global)
# • Name: 'Cedric Midoux'
# • Email: 'cedric.midoux@inrae.fr'
# • Global (user-level) gitignore file: '~/.gitignore'
# • Vaccinated: TRUE
# ℹ Defaulting to 'https' Git protocol
# • Default Git protocol: 'https'
# • Default initial branch name: <unset>
# GitHub
# • Default GitHub host: 'https://github.com'
# • Personal access token for 'https://github.com': <unset>
# • To create a personal access token, call `create_github_token()`
# • To store a token for current and future use, call `gitcreds::gitcreds_set()`
# ℹ Read more in the 'Managing Git(Hub) Credentials' article:
#   https://usethis.r-lib.org/articles/articles/git-credentials.html
# Git repo for current project
# ℹ No active usethis project


# Si besoin
usethis::use_git_config(user.name = "[nom]", user.email = "[email]")
```

3. Pour GitLab, on utilise la clé ssh publique (`Tools` > `Global Options` > `Git` > `View public key`) à renseigner sur son profil en ligne.

<!--https://gist.github.com/Z3tt/3dab3535007acf108391649766409421-->
4. [Depuis 2020](https://github.blog/2020-12-15-token-authentication-requirements-for-git-operations/), GitHub recommande l'utilisation de token personnel

```{r, eval=FALSE}
usethis::create_github_token()
# • Call `gitcreds::gitcreds_set()` to register this token in the local Git credential store
#   It is also a great idea to store this token in any password-management software that you use
# ✔ Opening URL 'https://github.com/settings/tokens/new?scopes=repo,user,gist,workflow&description=DESCRIBE THE TOKEN\'S USE CASE'

gitcreds::gitcreds_set()
```

5. Clonez votre projet :
    - New Project (en haut à droite)
    - Version Control
    - Git
    - Repository URL (clone with **HTTPS** : `https://github.com/<user>/<repository>.git`) et Subdirectory : `~/work/TRAINING/`

6. Explorez le dépôt depuis ce 3e device remote 

7. Complétez le .gitignore avec `usethis::git_vaccinate()`

7. Créez un document R Markdown :
    - File 
    - New file
    - R Markdown

**Dans ce document on a** :

- Une entête générale
- Du texte, mis en forme avec markdown
- Du code R dans des chunks
- Des résultats et outputs 

Grâce à l'onglet git de Rstudio, vous pouvez suivre l'état des fichiers, commit, diff, push/pull, ...

![](images/rstudio-git.png)

Chaque chunk peut être exécuté individuellement grâce à la flèche verte.  
Les options associées à chaque chunks sont disponible avec la roue crantée.

8. Modifier le document (en plusieurs commits)
    - ajoutez un chunk `knitr::kable(head(iris))` pour visualiser un table
    - ajoutez un chunk `plot(cars)` pour un plot
    - ajoutez un chunk bash `pwd` par exemple
    - ajoutez des commentaires mis en forme avec markdown
    - ajoutez une formule tel que `$\Delta = b^2 - 4ac$`

9. Lorsque vous êtes satisfait de votre rapport, générez la version HTML en cliquant sur `Knit`. Commitez, pushez, ...

10. Visualisez les modifications côté GitHub

11. Pour rentre une page HTML visualisable avec *GitHub Pages*
    - Dans les options du repo, dans le chapitre "GitHub Pages", activer la source correspondant à la branch `main` (ou master suivant votre configuration)
    - Le README.md est interprété et disponible à l'adresse https://[user].github.io/
    - La page HTML générée est disponible à l'adresse https://[user].github.io/[repo]/[page].html

**Votre rapport est disponible, versionné et partageable !**

Suivant l'interlocuteur, partagez le `.Rmd` ou le `.html`

# Intégration Continue

* Pour compiler a chaque modification les RMarkdown dans un environnement on utilise les *GitHub Actions*. On s'aide des templates de usethise ([liste d'exemples](https://github.com/r-lib/actions/tree/master/examples))

```{r eval=FALSE}
usethis::use_github_action("render-rmarkdown.yaml")
# ✔ Creating '.github/'
# ✔ Adding '^\\.github$' to '.Rbuildignore'
# ✔ Adding '*.html' to '.github/.gitignore'
# ✔ Creating '.github/workflows/'
# ✔ Saving 'r-lib/actions/examples/render-rmarkdown.yaml@v1' to '.github/workflows/render-rmarkdown.yaml'
# • Learn more at <https://github.com/r-lib/actions/blob/v1/examples/README.md>.
```

* Analysez le fichier créé `.github/workflows/render-rmarkdown.yaml`

* Installez rmarkdown et les autres packages nécessaires dans cet environnement
```{yaml}
      - name: Install rmarkdown
        run: Rscript -e 'install.packages("rmarkdown")'
      - name: Install ggplot2
        run: Rscript -e 'install.packages("ggplot2")'
      - name: Install dplyr
        run: Rscript -e 'install.packages("dplyr")'
```

* Pour une meilleur gestion des packages utilisé dans votre projet, il est conseillé d'utiliser [`{renv}`](https://rstudio.github.io/renv/index.html) (*The renv package helps you create reproducible environments for your R projects. Use renv to make your R projects more **isolated**, **portable** and **reproducible**.*)


* [Bug en cours de correction](https://github.com/r-lib/actions/issues/482), on regroupe les lignes :

```{yaml}
      - name: Render Rmarkdown files and Commit results
        run: |
          RMD_PATH=($(git diff --name-only ${{ github.event.before }} ${{ github.sha }} | grep '[.]Rmd$'))
          Rscript -e 'for (f in commandArgs(TRUE)) if (file.exists(f)) rmarkdown::render(f)' ${RMD_PATH[*]}
          git config --local user.name "$GITHUB_ACTOR"
          git config --local user.email "$GITHUB_ACTOR@users.noreply.github.com"
          git commit ${RMD_PATH[*]/.Rmd/.md} -m 'Re-build Rmarkdown files' || echo "No changes to commit"
          git push origin || echo "No changes to commit"
```

* Définir le format de sortie du Rmd

```{yaml}
---
title: "notebook"
output: github_document
---
```

* Sur GitHub, autorisez les modifications (commit) par le workflow : `Settings` > `Actions` > `Workflow permissions` > `Read and write permissions`

* Poussez les modifications

* Sur GitHub, explorez l'onglet *Actions*

> [Pour aller plus loin](https://orchid00.github.io/actions_sandbox/)  
> [Voir aussi](https://github.com/bbest/gh-actions-demo)

* On peut aussi définir le commit des html dans le `render-rmarkdown.yaml` et spécifier comme output   `html_document:` avec comme paramètres `self_contained: true`

## Pour GitLab

* Le pipeline est défini dans `.gitlab-ci.yml`

* On utilise une image Docker R de départ

* On déporte le knit sur les runners de la forge.

* Pour chaque modification poussée sur le dépôt, GitLab CI/CD exécute le pipeline qui publie immédiatement les modifications sur le site.

* exemple :

```{yaml}
image: rocker/verse:4.0.0

before_script:
  - R -e "install.packages('rmarkdown')"

pages:
  stage: deploy
  script:
    - Rscript -e "purrr::map(fs::dir_ls(regexp = '.Rmd$'), rmarkdown::render)"
    - mkdir .public
    - cp -r ./* .public
    - rm -rf public
    - mv .public public
  artifacts:
    paths:
      - public
  rules:
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
```

* A chaque commit les notebooks sont régénérés pour assurer une cohérence des résultats.

* On peut ajouter des tests, des builds, des déploiements, etc ...

---

# Complément : se connecter en ssh au cluster `@front` grace aux clés ssh

Comme l'on a utiliser les clés SSH pour se connecter à GitHub depuis front, il est possible de se connecter à front depuis votre ordinateur.

* On crée une nouvelle paire de clés
```{bash, eval=FALSE}
ssh-keygen -t rsa -b 4096 -C "your_email@example.com"
```

* On l'envoie de manière sécurisé en rentrant pour une dernière fois sa phrase de passe.
```{bash, eval=FALSE}
ssh-copy-id -i id_rsa.pub <login>@front.migale.inrae.fr
```

* Maintenant on peut se connecter plus facilement
```{bash, eval=FALSE}
ssh <login>@front.migale.inrae.fr
```

* La liste des clés autorisées est regroupé dans `~/.ssh/authorized_keys`

* *Le bonus du bonus* : utiliser des alias. On peut renseigner dans le fichier `.ssh/config` des paramètres de connexion tel le login et l'adresse du serveur. Voici un exemple de `.ssh/config` :

```
Host *
	ForwardAgent yes
	ForwardX11 yes
	ForwardX11Trusted yes
	ForwardX11Timeout 8h
	ServerAliveInterval 120

Host front
	HostName front.migale.inrae.fr
	User <login>
	
Host ifb
	HostName core.cluster.france-bioinformatique.fr
	User <login>
```

* En combinant ceci et les clés SSH, on peut se connecter avec :
```{bash, eval=FALSE}
ssh front
```

Vous pouvez mutualiser votre paire de clé depuis plusieurs terminaux mais la clé privée est extrêmement confidentielle et il faut être très vigilent aux fuites et pertes.

# References

* [Mémo Git](https://github.github.com/training-kit/downloads/github-git-cheat-sheet.pdf) et [VF](https://github.github.com/training-kit/downloads/fr/github-git-cheat-sheet.pdf)
* [Mémo Markdown](https://guides.github.com/pdfs/markdown-cheatsheet-online.pdf)
* [Mémo RMarkdown](https://www.rstudio.org/links/r_markdown_cheat_sheet)
* [Mémo Jupyter Notebook](https://datacamp-community-prod.s3.amazonaws.com/48093c40-5303-45f4-bbf9-0c96c0133c40)
