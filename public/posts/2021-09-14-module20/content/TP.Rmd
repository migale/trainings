---
title: "TP formation Metabarcoding"
author: "Olivier Rué, Cédric Midoux, Mahendra Mariadassou"
subtitle: "Plateforme Migale"
bibliography: biblio.bib
csl: biomed-central.csl
date: "`r Sys.Date()`"
output:
    html_document:
      css: [css/html_doc.css, 'https://use.fontawesome.com/releases/v5.0.9/css/all.css']
      self_contained: true
      number_sections: true
      code_folding: "hide"
      toc: true
      toc_depth: 3
      toc_float: true
      includes:
        after_body: footer.html
    
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(eval=FALSE, echo =FALSE, cache = FALSE, message = FALSE, warning = FALSE, cache.lazy = FALSE,
                      fig.height = 3.5, fig.width = 10.5)
```

```{r, eval=T, echo=F}
library(DT)
library(tidyverse)
```


```{r, eval=TRUE, echo=FALSE}
download.file("https://analyses.migale.inra.fr/resources/biblio.bib", "biblio.bib")
download.file("https://analyses.migale.inra.fr/resources/biomed-central.csl", "biomed-central.csl")
#download.file("https://analyses.migale.inra.fr/css/styles.css", "css/html_doc.css")
```


Ce document vous présente la démarche et la réflexion pour analyser un jeu de données de métabarcoding avec l'outil <strong class="tool">FROGS</strong> @frogs. Les analyses ont été effectuées sur [l'instance Galaxy](https://galaxy.migale.inrae.fr) de la plateforme [Migale](https://migale.inrae.fr) avec la **version 3.1.0**.

<hr>

# Introduction à Galaxy

## TP de découverte / rappels

Galaxy propose des tutoriels pour découvrir l'interface ou apprendre à utiliser certains outils. [Ce tutoriel](https://galaxyproject.github.io/training-material/topics/introduction/tutorials/galaxy-intro-short/tutorial.html) vous explique comment utiliser l'interface, importer des données et comment les organiser.

## Instance Galaxy de Migale

Connectez-vous sur l'instance Galaxy de la plateforme Migale : <a href="https://galaxy.migale.inrae.fr">galaxy.migale.inrae.fr</a>. Votre identifiant et votre mot de passe vous ont été communiqués dans le mail d'activation de votre compte sur la plateforme.

<div class="alert info">Si vous oubliez vos identifiants/mdp: help-migale@inrae.fr</div>


<details>
<summary>Solution pas à pas</summary>

<div>
<video controls width="500">
    <source src="media/connection.webm"
            type="video/webm">
    Sorry, your browser doesn't support embedded videos.
</video>
</div>

</details>

# Préparer les historiques

Créer les historiques `16S`, `ITS` et `MULTIPLEXED` et les remplir avec les fichiers associés :

* 16S
  - http://genome.jouy.inra.fr/formation/Metagenomics_03-2021/16S.tar.gz
* MULTIPLEXED
  - http://genome.jouy.inra.fr/formation/Metagenomics_03-2021/Multiplexed/barcode.tabular
  - http://genome.jouy.inra.fr/formation/Metagenomics_03-2021/Multiplexed/multiplex.fastq
* ITS
  - http://genome.jouy.inra.fr/formation/Metagenomics_03-2021/ITS1.tar.gz

<details>
<summary>Solution pas à pas</summary>

<div>
<video controls width="500">
    <source src="media/preparation_histories.webm"
            type="video/webm">
    Sorry, your browser doesn't support embedded videos.
</video>
</div>

</details>

Importez l'historique `16S ALL FASTQ` présent dans l'instance dans `Données partagées / Historiques` 

<details>
<summary>Solution pas à pas</summary>

<div>
<video controls width="500">
    <source src="media/import_history.webm"
            type="video/webm">
    Sorry, your browser doesn't support embedded videos.
</video>
</div>

</details>

## Contenu de l'historique MULTIPLEXED

## Contenu de l'historique 16S

Il s'agit de résultats issus du travail de [Chaillou et al., 2015](https://doi.org/10.1038/ismej.2014.202) comparant les communautés bactériennes de produits de la mer et de produits carnés.

![](images/chaillou_dataset.png)

Les séquences que vous allez analyser sont des données simulées générées à partir des abondances estimées dans le papier. 

* Les 64 échantillons correspondent à 8 réplicats des 8 produits différents, générés sur la région 16S V1-V3.
* Les primers sont 27F (5'-AGAGTTTGATCCTGGCTCAG-3') 534R (5'-ATTACCGCGGCTGCTGG-3')
* Des reads de 2x300 bp ont été générés
* Des erreurs ont été ajoutés dans les séquences suivant le modèle d'erreur Illumina classique
* 10% de séquences chimériques ont été ajoutées

Vous avez à dispostion un fichier de métadonnées :

```{r, eval=TRUE}
df <- read.csv2("data/metadata_16S.tsv", sep="\t") 
df %>% datatable(rownames= FALSE)
```

et une archive contenant les 64x2=128 fichiers FASTQ compressés.

## Contenu de l'historique 16S ALL FASTQ

Dans cet historique, vous retrouverez les 128 fichiers FASTQ hors de leur archive.

## Contenu de l'historique ITS

Vous avez ici un jeu de données ITS de fromages (real) et de communautés synthétiques. Il contient une archive contenant les fichiers FASTQ et les métadonnées associées :

```{r, eval=TRUE}
df <- read.csv2("data/metadata_ITS.tsv", sep="\t") 
df %>% datatable(rownames= FALSE)
```

Ce sont des données issues du projet INRAE MEM METABARFOOD (PRJNA685292) dont l'objectif est d'évaluer la pertinence des marqueurs eucaryotes classiquement utilisés pour caractériser les communautés des écosystèmes alimentaires d'intérêt à INRAE.

* Il s'agit ici d'échantillons de fromages réels (echantillon*), de MOCKs simples à complexes d'espèces d'intérêt
* Les primers sont ITS1-F (5'-CTTGGTCATTTAGAGGAAGTAA-3') ITS2 (5'-GCTGCGTTCTTCATCGATGC-3')
* Des reads de 2x250 bp ont été produits par Illumina Miseq


# MULTIPLEXED

## FROGS Demultiplex

Déplacez-vous dans l'historique `MULTIPLEXED`.

L'historique `MULTIPLEXED` contient un fichier FASTQ contenant des reads appartenant à différents échantillons. La correspondance est indiquée dans le fichier `barcode.tabular` :


| Sample | 5' seq | 3' seq |
| ------ | ------ | ------ |
| MgArd0001 | ACAGCGT | TGTACGT |
| MgArd0009 | ACAGTAG | TGTACGT |
| MgArd0017 | ACGTCAG | TGTACGT |
| MgArd0029 | ACTCAGT | TGTACGT |
| MgArd0038 | ACTCGTC | TGTACGT |
| MgArd0046 | AGCAGTC | TGTACGT |
| MgArd0054 | AGCTATG | TGTACGT |
| MgArd0062 | AGCTCGC | TGTACGT |
| MgArd0073 | AGTATCT | TGTACGT |
| MgArd0081 | AGTCTGC | TGTACGT |

La 1ère colonne correspond au nom de l'échantillon, la seconde à la séquence de barcode sur l'extrémité 5', la troisième à la séquence du barcode sur l'extrémité 3'.

Exemple :

<strong style="color:red">AGCTATG</strong>ACTGGGTGTAAGAGCTGTGATTGCTAACACTGTGGCCGGGCCAGGGCACCTGGATAAATCGGATTAGATACCCGGGTA<strong style="color:red">TGTACGT</strong>


<i class="fas fa-comments"></i> <strong>À quel échantillon sera assignée cette séquence ?</strong>

```{bash}
MgArd0054
```


Lancez l'outil <strong class="tool">FROGS - demultiplex reads</strong> et choisir les paramètres adaptés.


```{bash}
Barcode file : barcode.tabular
Single or Paired-end reads : Single
Select fastq dataset : multiplex.fastq
Barcode mismatches : 0
Barcode on which end? : Both ends
```

<details>
<summary>Solution pas à pas</summary>

<div>
<video controls width="500">
    <source src="media/frogs_demultiplex.webm"
            type="video/webm">
    Sorry, your browser doesn't support embedded videos.
</video>
</div>

</details>

<i class="fas fa-comments"></i> <strong>Pourquoi n'y a-t-il pas 10 fichiers FASTQ en sortie ?</strong>

```{bash}
Pour organiser les données, les 10 fichiers FASTQ sont rangés dans une archive appelée demultiplexed.tar.gz. Si vous l'ouvrez, vous verrez que les 10 fichiers y sont.
```

<i class="fas fa-comments"></i> <strong>Que deviennent les séquences dont les barcodes ne sont pas retrouvés ?</strong>

```{bash}
Elles sont stockées dans le dataset undemultiplexed.tar.gz qui contient les séquences non démultiplexées au format FASTQ.
```

<i class="fas fa-comments"></i> <strong>Pourquoi n'y a-t-il aucune séquence _ambigous_ ?</strong>

```{bash}
Car aucun mismatch n'a été toléré dans la séquence des barcodes. En autorisant des mismatches, on risque d'avoir des ambiguités entre plusieurs séquences, et le cas échéant l'outil ne sait pas décider entre les différents échantillons possibles. Ces séquences sont donc classées ambigous.
```

# 16S

## Quality control

### Vérifier la qualité des données de séquençage

Placez-vous dans l'historique `16S ALL FASTQ`

Lancez <strong class="tool">FastQC</strong> @fastqc pour analyser la qualité des 64 échantillons (128 fichiers)

<div class="alert comment">Il est possible de choisir plusieurs datasets en même temps pour leur appliquer la même action ou de créer une collection !</div>

<details>
<summary>Solution</summary>

<div>
<video controls width="500">
    <source src="media/fastqc.webm"
            type="video/webm">
    Sorry, your browser doesn't support embedded videos.
</video>
</div>

</details>

Lancez <strong class="tool">MultiQC</strong> @multiqc pour obtenir une rapport synthétique

<details>
<summary>Solution</summary>

<div>
<video controls width="500">
    <source src="media/multiqc.webm"
            type="video/webm">
    Sorry, your browser doesn't support embedded videos.
</video>
</div>

</details>

<i class="fas fa-comments"></i> <strong>Quel est le principal atout de cet outil ?</strong>

```{bash}
MultiQC permet d'aggréger une multitude de rapports en un seul. C'est très intéressant pour comparer les échantillons entre eux et constater très facilement s'ils sont semblables ou pas, en termes de nombre de reads, de qualité, de contenu...
```

Voilà un [rapport](data/exemple_multiqc.html) sur de vraies données qui va vous permettre d'être confrontés à des informations réelles.

<i class="fas fa-comments"></i> <strong>Est-ce que tous les reads sont de la même longueur ? Qu'est-ce que cela signifie ?</strong>

```{bash}
Non, les reads sont de longueur différente. Cela indique qu'un traitement a été effectué sur les données brutes. Il s'agit de comprendre pourquoi. Si vous les recevez ainsi, contactez la plateforme de séquençage pour obtenir des informations. Ici, les primers de séquençage ont été retirés.
```

<i class="fas fa-comments"></i> <strong>La qualité des bases vous semble-t-elle correcte ?</strong>

```{bash}
Les données de séquençage Illumina ont généralement des profils équivalents, avec une qualité de base qui se dégrade vers la fin du read.
```



## Préparer une archive pour FROGS

### Pourquoi ?

FROGS a besoin que tous les fichiers d'entrée lui soient fournis, mais contrairement aux collections, l'outil ne doît pas être lancé sur chaque fichier. Sans archive, il faut donc entrer tous les échantillons les uns après les autres. C'est fastidieux et peut causer des erreurs.

### Créer une archive

Une archive est une sorte de fichier qui contient plein de fichiers.

Cette archive doit avoir les propriétés suivantes :

* Elle ne doit contenir que les fichiers FASTQ, pas de dossiers ni d'autres fichiers
* Les fichiers FASTQ peuvent être compressés
* Les fichiers FASTQ doivent être suffixés, si les données sont pairées, par *_R1.fastq.gz* et *_R2.fastq.gz*
* Le nom de l'échantillon est construit à partir des chaines de caractères précédant ces suffixes.

En ligne de commande :

```{bash, eval=FALSE}
cd MyDir # les fichiers FASTQ sont dans ce répertoire
tar zcvf Archive.tar.gz *.fastq.gz
```

[Ce document](media/archive_creation_galaxy.pdf) explique comment créer une archive sous Windows.

Pour utiliser une archive dans <strong class="tool">FROGS preprocess</strong> il faut qu'elle soit uploadée au format <code>tar</code> (Datatype). Il suffit alors, dans le formulaire de choisir Archive dans la section <code>Input type</code>.

## FROGS Preprocess

Switchez dans l'historique `16S`

Lancez <strong class="tool">FROGS preprocess</strong>. Utilisez les informations que nous vous avons fournies pour déterminer quels paramètres utiliser, ainsi que la documentation en bas du formulaire de soumission.


```{bash}
Sequencer: Illumina
Input Type: Archive
Archive file: 16S.tar.gz
Reads already merged: No
Reads 1 size: 300
Reads 2 size: 300
mismatch rate: 0.1
merge software: vsearch
Would you like to keep unmerged reads?: No
Minimum amplicon size: 400
Maximum amplicon size: 580
Sequencing protocol: Illumina standard
5' primer: AGAGTTTGATCCTGGCTCAG
3' primer: CCAGCAGCCGCGGTAAT (!)
```


<details>
<summary>Solution pas à pas</summary>

<div>
<video controls width="500">
    <source src="media/frogs_preprocess.webm"
            type="video/webm">
    Sorry, your browser doesn't support embedded videos.
</video>
</div>

</details>

Explorez le rapport HTML généré.

<i class="fas fa-comments"></i> <strong>Quelle taille d'amplicons choisir ?</strong>

```{bash}
Quand vous ne savez pas, mettez des bornes basses et hautes puis allez regarder la distribution des séquences (bouton Display amplicon lengths) pour définir des bornes plus précises.
```


Lisez la documentation de l'outil (bas du formulaire) pour comprendre ce qui a été fait

<i class="fas fa-comments"></i> <strong>Quelles informations sont présentes dans le fichier FASTA ?</strong>

```{bash}
Dans le fichier FASTA sont stockées les séquences des amplicons mais pas seulement

Exemple:

>31_4222;size=30
AAAGCGCCGCTGAGGGATGGGCCTGCGTCCGATTAGCTTGTTGGTGGGGTAACGGCCCACCAAGGCGACGATCGGTAG...

31_4222 est le nom du read (dans la réalité il ressemble plus à M02765:8:000000000-AHM7P:1:1103:17002:15991)
size=4 indique le nombre de séquences identiques trouvées dans tous les échantillons
```

<i class="fas fa-comments"></i> <strong>Quelles informations sont présentes dans le fichier TSV ?</strong>

```{bash}
Ce fichier est la table d'abondance de chaque amplicon unique par échantillon

Exemple:

#id           BHT0.LOT01  BHT0.LOT3 
31_4222               10        20

L'amplicon est vu 10 fois dans l'échantillon BHT0.LOT01 et 20 fois dans l'autre échantillon.
```

<i class="fas fa-comments"></i> <strong>Est-ce attendu que certaines paires de reads ne contiguent pas ?</strong>

```{bash}
Tout dépend de la taille attendue de l'amplicon qui a été séquencé. Généralement, les régions du 16S ciblées sont censées permettre un chevauchement des paires. Dans ce cas précis, les paires qui ne contiguent pas ne sont pas attendues et peuvent (doivent) être supprimées.
Pour les ITS, il est essentiel de les conserver par contre car certains ITS peuvent dépasser 600 paires de bases.
Si la taille attendue de votre amplicon est supérieure à 600 bp, vous ne deviez pas avoir de paires qui contiguent.
```

<i class="fas fa-comments"></i> <strong>Qu'est-ce que la déréplication ?</strong>

```{bash}
La déréplication permet de garder un unique exemplaire de plusieurs séquences identiques. Le détail de l'abondance de cette séquence par échantillon est indiquée dans le fichier count.tsv
```


## FROGS Clustering

Lancez l'outil <strong class="tool">FROGS clustering</strong> en spécidiant une valeur de **d=1** et en ne réalisant pas l'étape de _denoising_.

```{bash}
Sequences file: FROGS preprocess FASTA file
Count file: FROGS preprocess TSV file
Aggregation distance: 1
Perform denoising clustering step: No
```


<details>
<summary>Solution pas à pas</summary>

<div>
<video controls width="500">
    <source src="media/frogs_clustering.webm"
            type="video/webm">
    Sorry, your browser doesn't support embedded videos.
</video>
</div>

</details>

<i class="fas fa-comments"></i> <strong>Quelles informations sont contenues dans les datasets générés ?</strong>

```{bash}
FASTA: séquences représentatives des OTUs construits

Exemple:

>Cluster_1 1:N:0:99
TACGTAGGTGGCAAGCGTTATCCGGAATTATTGGGCGTAAAGCG...

BIOM: fichier organisé de façon hiérarchique permettant de stocker entre autres les abondances des OTUs par échantillon

ne pas chercher à le lire à l'oeil

TSV: une ligne correspond à un OTU. L'ensemble des identifiants des séquences qui le composent est indiqué (en pratique on ne s'en occupe presque jamais)
```


<i class="fas fa-comments"></i> <strong>Qu'est-ce que l'étape de denoising ?</strong>

```{bash}
Le denoising consiste à effectuer un clustering à d=1 avant de faire un clustering avec un d plus important. Cela permet d'accélérer de façon significative le traitement sans trop changer le résultat.
```


Lancez l'outil <strong class="tool">FROGS clusters stat</strong>

<details>
<summary class="solution">Solution pas à pas</summary>

<div>
<video controls width="500">
    <source src="media/frogs_clusters_stat.webm"
            type="video/webm">
    Sorry, your browser doesn't support embedded videos.
</video>
</div>

</details>

Explorez le rapport HTML généré.

<i class="fas fa-comments"></i> <strong>Combien de séquences sont contenues dans le plus gros OTU ?</strong>

```{bash}
83,705
```

<i class="fas fa-comments"></i> <strong>Combien de clusters sont composés d'une seule séquence ?</strong>

```{bash}
50,811
```

<i class="fas fa-comments"></i> <strong>Quel pourcentage du total des OTUs représentent-ils ?</strong>

```{bash}
98.39%
```


## FROGS Remove chimera

Lancez l'outil <strong class="tool">FROGS remove chimera</strong> pour supprimer les éventuelles chimères que pourrait détecter <strong class="tool">vsearch</strong> @vsearch.

<details>
<summary>Solution pas à pas</summary>

<div>
<video controls width="500">
    <source src="media/frogs_chimera.webm"
            type="video/webm">
    Sorry, your browser doesn't support embedded videos.
</video>
</div>

</details>

Explorez le rapport HTML généré.

<i class="fas fa-comments"></i> <strong>Combien d'OTUs ont été supprimés ?</strong>

```{bash}
14,159 (27.4%)
```


<i class="fas fa-comments"></i> <strong>Combien cela représente-t-il de séquences ?</strong>

```{bash}
14,609 (2.6%)
```

<i class="fas fa-comments"></i> <strong>Qu'en concluez-vous ?</strong>

```{bash}
Les OTUs chimériques sont majoritairement composés de peu de séquences.
```

<i class="fas fa-comments"></i> <strong>Quelle est la plus grande abondance d'OTU chimérique détecté ?</strong>

```{bash}
99
```

## FROGS Filters

Lancez l'outil <strong class="tool">FROGS filters</strong> pour ne garder que les OTUs avec une abondance supérieure à **0.005%** et présents dans au moins **4** échantillons. Utilisez également la détection des séquences _phiX_.

<details>
<summary class="solution">Solution pas à pas</summary>

<div>
<video controls width="500">
    <source src="media/frogs_filters.webm"
            type="video/webm">
    Sorry, your browser doesn't support embedded videos.
</video>
</div>

</details>

Explorez le rapport HTML généré.

<i class="fas fa-comments"></i> <strong>Combien d'OTUs ont été supprimés et combien représentent-ils de séquences ?</strong>

```{bash}
98.7% des OTUs sont supprimés ! Mais cela représente seulement 7% des séquences.
```

<i class="fas fa-comments"></i> <strong>Quelle information est présente dans le fichier excluded.tsv ?</strong>

```{bash}
Chaque colonne représente un filtre et chaque ligne un OTU. Un OTU peut bien entendu se retrouver dans plusieurs colonnes.
```


<i class="fas fa-comments"></i> <strong>Quel(s) est (sont) le(s) filtre(s) qui a (ont) permis de supprimer le plus d'OTUs ?</strong>

```{bash}
La grande majorité des OTUs supprimés sont supprimés à la fois parce qu'ils sont vus dans moins de 4 échantillons et avec une abondance relative < 0.00005.
```

<i class="fas fa-comments"></i> <strong>Ces échantillons étaient-ils contaminés par des séquences de phiX restantes ?</strong>

```{bash}
Non
```



## FROGS Affiliation OTU


Lancez l'outil <strong class="tool">FROGS affiliation OTU</strong> pour effectuer une affiliation **Blast** avec la base de données **16S_SILVA_Pintail100_138**.

<details>
<summary class="solution">Solution pas à pas</summary>

<div>
<video controls width="500">
    <source src="media/frogs_affiliation.webm"
            type="video/webm">
    Sorry, your browser doesn't support embedded videos.
</video>
</div>

</details>

Explorez le rapport HTML généré.

<i class="fas fa-comments"></i> <strong>Est-ce que tous les OTUs ont été affiliés à une séquence présente dans la base de référence utilisée ?</strong>

```{bash}
Non aucune !
```


<i class="fas fa-comments"></i> <strong>Que feriez-vous des OTUs qui ne seraient pas affiliés ?</strong>

```{bash}
Il ne faut surtout pas les supprimer. Il s'agit peut-être de séquences non présentes dans la base de données utilisée. Dans ce cas, il faudrait utiliser d'autres bases de données (NCBI par exemple).
Il est important de savoir ce qu'elles sont. Elles peuvent être une contamination (hôte, environnement, amplification d'autres espèces...). Cela donnera des informations importantes sur la qualité de votre expérimentation.
```


<i class="fas fa-comments"></i> <strong>Expliquez le graphique <code>Blast multi-affiliation summary</code></strong>

```{bash}
Ce graphique représente le nombre d'OTUs / séquences multi-affiliées à un rang donné. Cela signifie que plusieurs hits identiques de l'OTU sont ressortis avec une ambiguité au rang suivant. Par exemple, pour une multi-affiliation au rang Genus, il y a deux hits d'espèces différentes.
```


<i class="fas fa-comments"></i> <strong>Le nombre de multi-affiliations au rang <code>Species</code> vous paraît-il surprenant ? Pourquoi ?</strong>

```{bash}
Non, le 16S ne permet généralement pas de discriminer les espèces au-delà du genre.
```

Lancez l'outil <strong class="tool">FROGS affiliation stats</strong> .

<details>
<summary>Solution pas à pas</summary>

<div>
<video controls width="500">
    <source src="media/frogs_affiliation_stat.webm"
            type="video/webm">
    Sorry, your browser doesn't support embedded videos.
</video>
</div>

</details>

Explorez le rapport HTML généré.

<i class="fas fa-comments"></i> <strong>Qu'est-ce qu'une courbe de raréfaction ? Quel est son intérêt principal ?</strong>

```{bash}
Une courbe de raréfaction permet d'évaluer si la profondeur de séquençage est suffisante. Cela permet de dire qu'on a suffisamment séquencé pour que la majorité des espèces de l'échantillon soient séquencées.
Il faut dans l'idéal que la courbe atteigne un plateau.
```



<i class="fas fa-comments"></i> <strong>À quoi vous ferait penser un OTU affilié avec un % de couverture inférieur à 80% ?</strong>

```{bash}
À une potentielle chimère !
```


<i class="fas fa-comments"></i> <strong>Que conclure si les % d'identité de vos OTUs les plus abondants sont faibles ?</strong>

```{bash}
Vos séquences d'intérêt ne sont pas présentes dans la base de données utilisée.
```


## FROGS Biom to TSV

Lancez l'outil <strong class="tool">FROGS biom to tsv</strong> en demandant l'extraction des mulit-affiliations et la présence des séquences dans le fichier de sortie.

```{bash}
Abundance file: FROGS affiliation OTU BIOM
Sequences file: FROGS filters FASTA
Extract multi-alignments: Yes
```


<details>
<summary class="solution">Solution pas à pas</summary>

<div>
<video controls width="500">
    <source src="media/frogs_biomtotsv.webm"
            type="video/webm">
    Sorry, your browser doesn't support embedded videos.
</video>
</div>

</details>

Explorez les datasets générés

<i class="fas fa-comments"></i> <strong>Combien de séquences possède le plus gros OTU ?</strong>

```{bash}
83,705 séquences
```

<i class="fas fa-comments"></i> <strong>Combien de séquences possède le moins gros OTU ?</strong>

```{bash}
28 séquences
```

<i class="fas fa-comments"></i> <strong>Pourquoi le Cluster_1 a plusieurs affiliations dans le fichier multi-affiliations sans pour autant être multi-affilié ?</strong>


```{bash}
Le Cluster_1 est affilié exclusivement à Brochotrix thermosphacta. Vous vous rappelez les copies de 16S dans les génomes bactériens ? Il n'y pas d'ambiguité sur l'espèce, donc pas de multi-affiliation ici.
```

<i class="fas fa-comments"></i> <strong>Que pensez-vous des affiliations du Cluster_3 ?</strong>

```{bash}
Le CLuster_3 est quasi-exclusivement affilié à l'espèce Lactobacillus sakei. Seule une affiliation (Bacteria;Firmicutes;Bacilli;Lactobacillales;Lactobacillaceae;Lactobacillus;unknown species) est contraire. Cette affiliation pourrait donc être légitimement changée. 
```

Pour vous faciliter la vie, nous avons développé une interface web : [AffiliationExplorer](https://shiny.migale.inrae.fr/app/affiliationexplorer) qui vous permet de modifier certaines multi-affiliations si nécessaire. Pour cela, il vous faudra télécharger sur votre ordinateur le fichier BIOM et le fichier de multi-affiliations, ainsi que le FASTA si vous avez éventuellement besoin de la séquence pour faire vos modifications.

<details>
<summary class="solution">Demonstration pour choisir une affiliation parmi les multi-affiliations</summary>

<div>
<video controls width="500">
    <source src="media/affiliationexplorer.webm"
            type="video/webm">
    Sorry, your browser doesn't support embedded videos.
</video>
</div>

</details>

<details>
<summary class="solution">Demonstration pour modifier une affiliation existante</summary>

<div>
<video controls width="500">
    <source src="media/affiliationexplorer2.webm"
            type="video/webm">
    Sorry, your browser doesn't support embedded videos.
</video>
</div>

</details>

## FROGS Tree

Lancez l'outil <strong class="tool">FROGS tree</strong>

<details>
<summary class="solution">Solution pas à pas</summary>

<div>
<video controls width="500">
    <source src="media/frogs_tree.webm"
            type="video/webm">
    Sorry, your browser doesn't support embedded videos.
</video>
</div>

</details>

Explorez le rapport HTML généré.

<i class="fas fa-comments"></i> <strong>Que faire si certains OTUs ne sont pas inclus dans l'arbre ?</strong>

```{bash}
Ceux-là seront inexploitables pour calculer des indices basés sur la distance phylogénétique.
```

# ITS

## FROGS Preprocess

Switchez dans l'historique `16S`

Lancez <strong class="tool">FROGS preprocess</strong>. Utilisez les informations que nous vous avons fournies pour déterminer quels paramètres utiliser, ainsi que la documentation en bas du formulaire de soumission.

<i class="fas fa-comments"></i> <strong>Quelle taille d'amplicons choisir ?</strong>

```{bash}
Pour les ITS, les tailles attendues vont de quelques dizaines de nucléotides à plusieurs kb ! Notre expérience nous a montré qu'entre 50 et 490 (ou 590 pour du 2x300), tous les ITS sont conservés.
```

```{bash}
Sequencer: Illumina
Input Type: Archive
Archive file: ITS1.tar.gz
Reads already merged: No
Reads 1 size: 250
Reads 2 size: 250
mismatch rate: 0.1
merge software: vsearch
Would you like to keep unmerged reads?: Yes
Minimum amplicon size: 50
Maximum amplicon size: 490
Sequencing protocol: Illumina standard
5' primer: CTTGGTCATTTAGAGGAAGTAA
3' primer: GCATCGATGAAGAACGCAGC (!)
```


<details>
<summary>Solution pas à pas</summary>

<div>
<video controls width="500">
    <source src="media/frogs_ITS_preprocess.webm"
            type="video/webm">
    Sorry, your browser doesn't support embedded videos.
</video>
</div>

</details>

Explorez le rapport HTML généré.

Lancez ensuite <strong class="tool">FROGS clustering</strong>, <strong class="tool">FROGS clusters stats</strong>, <strong class="tool">FROGS remove chimera</strong> et <strong class="tool">FROGS filters</strong> avec la même réflexion que pour les données 16S.

Explorez les rapports HTML générés pour vérifier que tout s'est bien passé.

## FROGS ITSx

Lancez <strong class="tool">FROGS ITSx</strong> sans utiliser l'option permettant de tronquer les séquences de SSU et de 5.8S.

```{bash}
Sequences file: FROGS filters FASTA
Abundance file: FROGS filters BIOM
ITS region: ITS1
Check only if sequence detected as ITS?: Yes
```

<details>
<summary>Solution pas à pas</summary>

<div>
<video controls width="500">
    <source src="media/frogs_ITS_preprocess.webm"
            type="video/webm">
    Sorry, your browser doesn't support embedded videos.
</video>
</div>

</details>

Explorez le rapport HTML généré.


## FROGS Affiliation OTU

Lancez <strong class="tool">FROGS Affiliation OTU</strong> en spécifiant la base de données `ITS_UNITE_Fungi_8.0` et sans faire d'assignation RDP.


```{bash}
Reference database: ITS_UNITE_Fungi_8.0
Also perform RDP assignation: No
OTU seed sequence: FROGS ITSx FASTA
Abundance file: FROGS ITSx BIOM
```

<details>
<summary>Solution pas à pas</summary>

<div>
<video controls width="500">
    <source src="media/frogs_ITS_affiliation.webm"
            type="video/webm">
    Sorry, your browser doesn't support embedded videos.
</video>
</div>

</details>

Explorez le rapport HTML généré.

Lancez ensuite <strong class="tool">FROGS Affiliations stats</strong> et <strong class="tool">FROGS Biom to TSV </strong> avec la même réflexion que pour les données 16S.

Explorez le rapport HTML généré.

<i class="fas fa-comments"></i> <strong>Comment expliquez-vous les statistiques d'alignement représentées dans le graphique __Alignment distribution__ du rapport HTML de FROGS Affiliations Stats ?</strong>

```{bash}
Le % de couverture est plus bas par rapport aux données 16S vues précédemment. Deux possibilités:
- soit les régions flanquantes ne sont pas dans la base de données
- soit la base de données est trop éloignée pour que les alignements soient bons
```

<i class="fas fa-comments"></i> <strong>Comment jugez-vous l'affiliation de l'OTU majoritaire (Cluster_1) ?</strong>

```{bash}
67% de couverture est trop faible pour être "normal". Dans ces cas-là, il faut utiliser une autre base de données pour rechercher la vraie taxonomie de cette séquence (si elle se trouve quelque part). Il faudra donc passer du temps à expertiser les affiliations "à la main".
```

<i class="fas fa-comments"></i> <strong>Est-ce que de longs ITS sont retrouvés ?</strong>

```{bash}
Non, ils auraient pu être reconnus grâce au tag "FROGS_combined" dans le nom de l'OTU.
```

# Références