---
title: "TP formation Metabarcoding"
author: "Olivier Rué, Cédric Midoux, Mahendra Mariadassou"
subtitle: "Plateforme Migale"
#css: css/styles.css
date: "2020/09/21"
output:
    html_document:
      css: [css/html_doc.css, 'https://use.fontawesome.com/releases/v5.0.9/css/all.css']
      self_contained: true
      number_sections: true
      code_folding: "show"
      toc: true
      toc_depth: 3
      toc_float: true
      includes:
        after_body: footer.html
    
---


Ce document vous présente la démarche et la réflexion pour analyser un jeu de données de métabarcoding avec l'outil FROGS. Les analyses ont été effectuées sur [l'instance Galaxy](https://galaxy.migale.inrae.fr) de la plateforme [Migale](https://migale.inrae.fr)

<hr>

# Introduction à Galaxy

## TP de découverte

Galaxy propose des tutoriels pour découvrir l'interface ou apprendre à utiliser certains outils. [Ce tutoriel](https://galaxyproject.github.io/training-material/topics/introduction/tutorials/galaxy-intro-short/tutorial.html) vous explique comment utiliser l'interface, importer des données et comment les organiser.

* Connectez-vous d'abord sur l'instance Galaxy de la plateforme Migale : <a href="https://galaxy.migale.inrae.fr">galaxy.migale.inrae.fr</a>. Votre identifiant et votre mot de passe vous ont été communiqués dans le mail d'activation de votre compte sur la plateforme.


# Préparer les historiques avec des données de métabarcoding déjà présentes dans Galaxy

Les historiques contenant les fichiers que nous allons utiliser sont préparés dans la section <code>Shared data</code>.
Vous devez créer 2 historiques différents avec le contenu suivant :

* Multiplexed
  - barcode.tabular
  - multiplex.fastq
* Miseq_R1_R2
  - sampleA_R1.fastq
  - sampleB_R2.fastq

<details>
<summary>Solution pas à pas</summary>

<div>
<video controls width="500">
    <source src="media/frogs_import_histories_from_datalibraries.webm"
            type="video/webm">
    Sorry, your browser doesn't support embedded videos.
</video>
</div>

</details>

# FROGS Demultiplex

* Se déplacer dans l'historique Multiplexed

L'historique <code>Multiplexed</code> contient un fichier FASTQ contenant des reads appartenant à différents échantillons. La correspondance est indiquée dans le fichier <code>barcode.tabular</code> :

```{bash, eval=F, echo=T}
MgArd0001	ACAGCGT	TGTACGT
MgArd0009	ACAGTAG	TGTACGT
MgArd0017	ACGTCAG	TGTACGT
MgArd0029	ACTCAGT	TGTACGT
MgArd0038	ACTCGTC	TGTACGT
MgArd0046	AGCAGTC	TGTACGT
MgArd0054	AGCTATG	TGTACGT
MgArd0062	AGCTCGC	TGTACGT
MgArd0073	AGTATCT	TGTACGT
MgArd0081	AGTCTGC	TGTACGT
```



* Lancer l'outil <strong class="tool">FROGS - demultiplex reads</strong>



<details>
<summary>Choix des paramètres</summary>

* Barcode file : <code>barcode.tabular</code>
* Single or Paired-end reads : Single
* Select fastq dataset : <code>multiplex.fastq</code>
* Barcode mismatches : 0
* Barcode on which end? : Both ends

</details>

<details>
<summary>Solution pas à pas</summary>

<div>
<video controls width="500">
    <source src="media/frogs_demultiplex.webm"
            type="video/webm">
    Sorry, your browser doesn't support embedded videos.
</video>
</div>

</details>

La 1ère colonne correspond au nom de l'échantillon, la seconde à la séquence de barcode sur l'extrémité 5', la troisième à la séquence du barcode sur l'extrémité 3'.

Exemple :

<strong style="color:red">AGCTATG</strong>ACTGGGTGTAAGAGCTGTGATTGCTAACACTGTGGCCGGGCCAGGGCACCTGGATAAATCGGATTAGATACCCGGGTA<strong style="color:red">TGTACGT</strong>

<details>
<summary>À quel échantillon sera assignée cette séquence ?</summary>

</details>

<details>
<summary>Pourquoi n'y a-t-il pas 10 fichiers FASTQ en sortie ?</summary>

</details>

<details>
<summary>Que deviennent les séquences dont les barcodes ne sont pas retrouvés ?</summary>

</details>

<details>
<summary>Pourquoi n'y a-t-il aucune séquence ambigous ?</summary>

</details>

# Quality control

## Descriptif des données

Les données que nous allons vous présenter ici sont des données issues de la [publication](https://journals.plos.org/plosone/article?id=10.1371/journal.pone.0227434#pone-0227434-g001) de Prodan et collaborateurs.
Nous allons utiliser les séquences issues de 3 Mocks. La région V4 du 16S a été ciblée pour 20 souches bactériennes, en suivant ce protocole :

<cite>
Genomic DNA from the Microbial Mock Community B (Even, Low concentration), v5.1L (Catalog no. HM-782D, obtained through BEI Resources, NIAID, NIH as part of the Human Microbiome Project) was sequenced in three separate runs. Details of mock composition are included in S1 Table. The mock contains DNA from 20 bacterial strains in equimolar (Even) ribosomal RNA operon counts (100000 copies per organism per μL). Two of the strains (Bacteriodes vulgatus and Clostridium beijerinckii) have multiple sequence variants in the V4 region of the 16S rRNA gene. B. vulgatus has three variants (in a 5:1:1 ratio), whereas C. beijerinckii has two variants (in a 13:1 ratio). The 16S rRNA sequences of Staphylococcus aureus and Staphylococcus epidermidis are identical in the V4 region. Therefore, the mock contains a total of 22 variants (ASVs) of the 16S gene in the V4 region. These sequences correspond to 19 OTUs when clustered at 97% identity. The mock community was sequenced three times in different sequencing runs. The mock raw sequence data is publicly available (https://github.com/andreiprodan/mock-sequences).
</cite>

* Créer l'historique <code>Mock</code> et importer les fichiers FASTQ
  - Mock_1_R1.fastq.gz
  - Mock_1_R2.fastq.gz
  - Mock_2_R1.fastq.gz
  - Mock_2_R2.fastq.gz
  - Mock_3_R1.fastq.gz
  - Mock_3_R2.fastq.gz

<details>
<summary>Solution pas à pas</summary>

<div>
<video controls width="500">
    <source src="media/frogs_import_mock.webm"
            type="video/webm">
    Sorry, your browser doesn't support embedded videos.
</video>
</div>

</details>

* Lancer l'outil <strong class="tool">FastQC</strong> sur chaque dataset

<div class="alert comment">Il est possible de choisir plusieurs datasets en même temps pour leur appliquer la même action !</div>

<details>
<summary>Solution pas à pas</summary>

<div>
<video controls width="500">
    <source src="media/frogs_fastqc.webm"
            type="video/webm">
    Sorry, your browser doesn't support embedded videos.
</video>
</div>

</details>


* Explorer le contenu des rapports web

<details>
<summary>Est-ce que tous les reads sont de la même longueur ? Qu'est-ce que cela signifie ?</summary>

</details>

<details>
<summary>La qualité des bases vous semble-t-elle correcte ?</summary>

</details>

* Lancer l'outil <strong class="tool">MultiQC</strong> à partir de chaque dataset <code>Raw Data</code>

<details>
<summary>Solution pas à pas</summary>

<div>
<video controls width="500">
    <source src="media/frogs_multiqc.webm"
            type="video/webm">
    Sorry, your browser doesn't support embedded videos.
</video>
</div>

</details>

* Explorer le rapport web obtenu

<details>
<summary>Quel est le principal atout de cet outil ?</summary>

</details>

---

# FROGS Preprocess

* Switcher dans l'historique <code>Mock</code> si besoin

* Lancer <strong class="tool">FROGS preprocess</strong> sur les 3 échantillons.
* Réflichir aux paramètres à utiliser sachant que :
  - La région V4 du 16S fait environ 250 nt
  - Les primers ont déjà été retirés des fichiers

<details>
<summary>Solution pas à pas</summary>

<div>
<video controls width="500">
    <source src="media/frogs_preprocess_fastq.webm"
            type="video/webm">
    Sorry, your browser doesn't support embedded videos.
</video>
</div>

<div class="alert danger">Cette solution n'est pas à privilégier. Rentrer tous les échantillons les uns à la suite des autres est source d'erreurs et une perte de temps.</div>

</details>

## Créer une archive

Il est conseillé avant d'uploader les fichiers FASTQ de chaque échantillon dans Galaxy de les stocker dans une archive. C'est une sorte de fichier qui contient plein de fichiers.

Cette archive doit avoir les propriétés suivantes :

* Elle ne doit contenir que les fichiers FASTQ, pas de dossiers ni d'autres fichiers
* Les fichiers FASTQ peuvent être compressés
* Les fichiers FASTQ doivent être suffixés, si les données sont pairées, par *_R1.fastq.gz* et *_R2.fastq.gz*
* Le nom de l'échantillon est construit à partir des chaines de caractères précédant ces suffixes.

En ligne de commande :

```{bash, eval=FALSE}
cd MyDir # les fichiers FASTQ sont dans ce répertoire
tar zcvf Archive.tar.gz *.fastq.gz
```

[Ce document](media/archive_creation_galaxy.pdf) explique comment créer une archive sous Windows.

</details>

## FROGS preprocess avec une archive

Pour utiliser une archive dans <strong class="tool">FROGS preprocess</strong> il faut qu'elle soit uploadée au format <code>tar</code> (Datatype). Il suffit alors, dans le formulaire de choisir Archive dans la section <code>Input type</code>.

## Exploration des datasets générés

* Lire la documentation de l'outil (bas du formulaire) pour comprendre ce qui a été fait

<details>
<summary>Quelles informations sont présentes dans le fichier FASTA ?</summary>
</details>

<details>
<summary>Quelles informations sont présentes dans le fichier TSV ?</summary>
</details>

<details>
<summary>Est-ce attendu que certaines paires de reads ne contiguent pas ?</summary>
</details>

<details>
<summary>Est-ce utile de conserver des reads qui ne sont pas contigués ?</summary>
</details>

<details>
<summary>Qu'est-ce que la déréplication ?</summary>

</details>

<details>
<summary>Quelle est votre analyse de cette étape ? </summary>
</details>


## FROGS preprocess avec des données pairées contenant encore les primers

* Switcher dans l'historique <code>Miseq_R1_R2</code>
* Lancer <strong class="tool">FROGS preprocess</strong> en renseignant le formulaire à partir des infos suivantes :
  - Les données sont du Miseq Illumina 2x250
  - Le primer en 5' : CCGTCAATTC
  - Le primer en 3' : AGCAGCNGCGG
  - 16S V3-V4 : longueur attendue entre 380 et 430 nt environ

<div class="alert comment">Attention, la séquence du primer 3' doit être reverse-complémentée avant d'être renseignée dans le formulaire. En effet, la séquence du primer est recherchée sur le fragment contigué, et pour cela, le R2 a été reverse-complémenté.</div>

<div class="alert comment">Attention, les primers sont inclus dans le calcul de la longueur des amplicons pour filtrer.</div>

# FROGS Clustering

* Switcher dans l'historique Mock
* Lancer l'outil <strong class="tool">FROGS clustering</strong> avec ces paramètres:
  - d = 3
  - Perform denoising step

<details>
<summary>Solution pas à pas</summary>

<div>
<video controls width="500">
    <source src="media/frogs_clustering.webm"
            type="video/webm">
    Sorry, your browser doesn't support embedded videos.
</video>
</div>

</details>

<details>
<summary>Quelles informations sont contenues dans les datasets générés ?</summary>
</details>
  
<details>
<summary>Qu'est-ce que l'étape de denoising ?</summary>
</details>

<details>
<summary>Qu'est-ce que le format BIOM ? Quelles informations reconnaissez-vous si vous l'ouvrez ?</summary>

</details>

<details>
<summary>Quelles informations sont contenues dans le fichier swarms_composition.tsv ?</summary>

</details>

* Lancer l'outil <strong class="tool">FROGS clusters stat</strong>

<details>
<summary class="solution">Solution pas à pas</summary>

<div>
<video controls width="500">
    <source src="media/frogs_clusters_stat.webm"
            type="video/webm">
    Sorry, your browser doesn't support embedded videos.
</video>
</div>

</details>

<details>
<summary>Combien de séquences sont contenues dans le plus gros OTU ?</summary>

</details>

<details>
<summary>Combien de clusters sont composés d'une seule séquence ?</summary>

</details>

<details>
<summary>Quel pourcentage du total des OTUs représentent-ils ?</summary>

</details>

<details>
<summary>Les trois échantillons ont-ils une composition en OTUs identique ?</summary>
</details>


# FROGS Remove chimera

* Lancer l'outil <strong class="tool">FROGS remove chimera</strong>

<details>
<summary>Solution pas à pas</summary>

<div>
<video controls width="500">
    <source src="media/frogs_chimera.webm"
            type="video/webm">
    Sorry, your browser doesn't support embedded videos.
</video>
</div>

</details>

<details>
<summary>Combien d'OTUs ont été supprimés ? Combien cela représente-t-il en nombre de séquences ?</summary>

</details>

# FROGS Filters

<details>
<summary class="solution">Solution pas à pas</summary>

<div>
<video controls width="500">
    <source src="media/frogs_filters.webm"
            type="video/webm">
    Sorry, your browser doesn't support embedded videos.
</video>
</div>

</details>

<details>
<summary>Combien d'OTUs ont été supprimés et combien représentent-ils de séquences ? </summary>

</details>

<details>
<summary>Quel(s) est (sont) le(s) filtre(s) qui a (ont) permis de supprimer le plus d'OTUs ?</summary>

</details>

<details>
<summary>Ces échantillons étaient-ils contaminés par des séquences de phiX restantes ?</summary>

</details>

# FROGS affiliation

<details>
<summary class="solution">Solution pas à pas</summary>

<div>
<video controls width="500">
    <source src="media/frogs_affiliation.webm"
            type="video/webm">
    Sorry, your browser doesn't support embedded videos.
</video>
</div>

</details>


<details>
<summary>Est-ce que tous les OTUs ont été affiliés à une séquence présente dans la base de référence utilisée ?</summary>

</details>

<details>
<summary>Que feriez-vous des OTUs qui ne seraient pas affiliés ?</summary>

</details>

<details>
<summary>Expliquez le graphique <code>Blast multi-affiliation summary</code></summary>

</details>

<details>
<summary>Le nombre de multi-affiliations au rang <code>Species</code> vous paraît-il surprenant ? Pourquoi ?</summary>

</details>

<details>
<summary>Solution pas à pas</summary>

<div>
<video controls width="500">
    <source src="media/frogs_affiliation_stat.webm"
            type="video/webm">
    Sorry, your browser doesn't support embedded videos.
</video>
</div>

</details>

<details>
<summary>Qu'est-ce qu'une courbe de raréfaction ? Quel est son intérêt principal ?</summary>

</details>

<details>
<summary>À quoi vous ferait penser un OTU affilié avec un % de couverture inférieur à 80% ?</summary>

</details>

<details>
<summary>Que conclure si les % d'identité de vos OTUs les plus abondants sont faibles ?</summary>

</details>

<details>
<summary>Que faire des séquences qui ne sont pas affiliées ?</summary>

</details>

# FROGS Biom to TSV

* Lancer l'outil <strong class="tool">FROGS biom to tsv</strong>

<details>
<summary class="solution">Solution pas à pas</summary>

<div>
<video controls width="500">
    <source src="media/frogs_biomtotsv.webm"
            type="video/webm">
    Sorry, your browser doesn't support embedded videos.
</video>
</div>

</details>

* Explorer les datasets générés

<details>
<summary>Combien de séquences possède le plus gros OTU ?</summary>

</details>

<details>
<summary>Combien de séquences possède le moins gros OTU ?</summary>

</details>

<details>
<summary>Trouvez un OTU pour lequel il serait intéressant de changer son affiliation</summary>

</details>

Pour ce faire, il faut :

* Télécharger le fichier <code>multi_hits.tsv</code> 
* Garder uniquement la ligne que vous voulez garder pour l'affiliation du Cluster_?
* Uploader le fichier dans Galaxy
* Utiliser l'outil <strong class="tool">FROGS tsv to biom</strong> en choisissant le fichier multi-affiliation précédemment uploadé
* Relancer <strong class="tool">FROGS affiliations_stat</strong> pour vérifier que la taxonomie a bien changé.


# FROGS tree

<details>
<summary class="solution">Solution pas à pas</summary>

<div>
<video controls width="500">
    <source src="media/frogs_tree.webm"
            type="video/webm">
    Sorry, your browser doesn't support embedded videos.
</video>
</div>

</details>


<details>
<summary>Que faire si certains OTUs ne sont pas inclus dans l'arbre ?</summary>

</details>

# Conclusions

```{r, eval=TRUE, echo=FALSE}
library(kableExtra)
file  <- read.csv2(file = "mock_results.tsv", sep = "\t", header = TRUE, stringsAsFactors = TRUE)
kable(file)
```

