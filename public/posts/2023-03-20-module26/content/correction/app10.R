#
# This is a Shiny web application. You can run the application by clicking
# the 'Run App' button above.
#
# Find out more about building applications with Shiny here:
#
#    http://shiny.rstudio.com/
#

library(shiny)

# Define UI for application that draws a histogram
ui <- fluidPage(

    # Application title
    titlePanel(textOutput("title")),
    
    # Sidebar with a slider input for number of bins 
    sidebarLayout(
        sidebarPanel(
            textInput("title",
                      "Main title :",
                      "Data visualization"),
            selectInput("data",
                        "Select data input :",
                        choices = c("Old Faithful Geyser Data" = "faithful",
                                    "Iris Data" = "iris",
                                    "Motor Trend Car Road Tests" = "mtcars",
                                    "Normally distributed random numbers" = "random",
                                    "My own data" = "upload")),
            conditionalPanel(
                condition = "input.data == 'upload'",
                fileInput("file",
                          "Choose CSV File", 
                          accept = ".csv")),
            sliderInput("bins",
                        "Number of bins:",
                        min = 1,
                        max = 50,
                        value = 30),
            radioButtons("color",
                         "Color of bins:",
                         choices = c("Dark gray" = "darkgray",
                                     "Orange" = "coral",
                                     "Light green" = "lightgreen")),
            conditionalPanel(
                condition = "input.data == 'random'",
                numericInput("randomN",
                             "Number of observations :",
                             value = 100,
                             min = 1),
                numericInput("randomMean",
                             "Mean :",
                             value = 0),
                numericInput("randomSd",
                             "Standard deviation :",
                             value = 1)
            )
        ),

        # Show a plot of the generated distribution
        mainPanel(
            tabsetPanel(
                tabPanel(
                    "Histogram",
                    plotOutput("histogram"),
                    textOutput("textBins")
                ),
                tabPanel(
                    "Boxplot",
                    plotOutput("boxplot"),
                ),
                tabPanel(
                    "Table",
                    tableOutput("table"),
                )
            )
        )
    )
)

# Define server logic required to draw a histogram
server <- function(input, output) {
    output$title <- renderText({
        input$title
    })
    
    datasetInput <- reactive({
        switch(input$data,
               "faithful" = faithful$waiting,
               "iris" = iris$Petal.Length,
               "mtcars" = mtcars$hp,
               "random" = rnorm(n = input$randomN, mean = input$randomMean, sd = input$randomSd),
               "upload" = read.csv(input$file$datapath, header = FALSE)[,1])
    })
    
    output$histogram <- renderPlot({
        # generate bins based on input$bins from ui.R
        x    <- datasetInput()
        bins <- seq(min(x), max(x), length.out = input$bins + 1)

        # draw the histogram with the specified number of bins
        hist(x, breaks = bins, col = input$color, border = 'white')
    })
    
    output$textBins <- renderText({
        c("There are currently", input$bins, input$color, "bins.")
    })
    
    
    output$boxplot <- renderPlot({
        x    <- datasetInput()
        boxplot(x, col = input$color)
    })
    
    output$table <- renderTable({
        x    <- datasetInput()
        x
    })
}

# Run the application 
shinyApp(ui = ui, server = server)
